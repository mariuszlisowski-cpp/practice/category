// christmas tree

#include <iostream>
#include <string>

void drawTree(int height) {
    for (int i = 0; i < height; i++) {
        int spaces = (height - i) - 1;
        int stars = (2 * i) + 1;
        std::string sSpace(spaces, ' ');
        std::cout << sSpace << std::string(stars, '*')
                  << sSpace << '\n';
    }
}

int main() {
    drawTree(7);

    return 0;
}
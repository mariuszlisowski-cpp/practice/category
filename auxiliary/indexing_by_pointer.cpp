/*
Indexing by pointer
#ponter #list_initialization
(compile with '-std=c++11' option)
*/

#include <iostream>

using std::cout;
using std::endl;
using std::string;

int main()
{
  int arr[]{ 0, 1, 2, 3 ,4 ,5, 6, 7, 8, 9 }; // C++ 11 extension

  /* indeksowanie zwykłe */
  for (int i = 0; i < 10; i++)
    cout << arr[i];
  cout << endl;

  /* indeksowanie wskaźnikiem */
  for (int i = 0; i < 10; i++)
    cout << *(arr + i);
  cout << endl;

  return 0;
}

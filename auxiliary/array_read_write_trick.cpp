// 

#include <iostream>

int main() {
    const size_t SIZE = 10;
    int* dynamicArray = new int[SIZE];

    // write
    for (size_t i = 0; i < SIZE; i++) {
        *(dynamicArray + i) = i;
        dynamicArray[i] = i; // same as above (overwrites)
        i[dynamicArray] = i;  // same as above (overwrites)
    }

    // read
    for (size_t i = 0; i < SIZE; i++) {
        std::cout << dynamicArray[i] << std::endl; // (adding i to array address)
        std::cout << i[dynamicArray] << std::endl; // same as above (reversed adding)
        std::cout << *(dynamicArray + i) << std::endl; // same as above
    }

    return 0;
}
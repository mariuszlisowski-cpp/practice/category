#include <iostream>
using namespace std;

void swapOneLiner(int& a, int& b) {
    a += b -= a = b - a;
    cout << "> swapped" << endl;
}

void swapXOR (int& a, int& b) {
    (a ^= b), (b ^= a), (a ^= b);
    cout << "> swapped" << endl;
}

int main()
{
    int a = 43, b = 44;
    if (a < b) {
        swapOneLiner(a, b);
        // swapXOR(a, b);
    }
    if (a != b) {
        cout << "Number " << a << " is bigger than " << b << endl;
    } else {
        cout << "Numbers are equal" << endl;
    }
}

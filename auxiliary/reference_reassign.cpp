#include <iostream>

int main() {
    int x = 9;

    int& x_ref = x;
    std::cout << "x_ref value: " << x_ref << std::endl;

    int y = 7;
    x_ref = y;                                                  // reassigning the value
                                                                // NOT rebinding the reference

    std::cout << "x_ref value: " << x_ref << std::endl;         // new value of...
    std::cout << "x value    : " << x << std::endl;             // x

    std::cout << "x_ref address: " << &x_ref << std::endl;      // same addres of reference as...
    std::cout << "x address    : " << &x << std::endl;          // x address

    return 0;
}

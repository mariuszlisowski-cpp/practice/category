// random vector generator
// #mt19937 #random_device #uniform_int_distribution #generate

#include <iostream>
#include <algorithm>
#include <vector>
#include <random>

constexpr int MIN = 1;
constexpr int MAX = 9;
constexpr int VEC_SIZE = 20;

int generateRandom();

int main() {
    std::vector<int> vec(VEC_SIZE);

    // generate
    std::generate(vec.begin(), vec.end(), generateRandom);

    // display
    for (auto e : vec)
        std::cout << e << "|";
    std::cout << std::endl;

    return 0;
}

int generateRandom() {
    static std::random_device rd;
    static std::mt19937 generator(rd());

    std::uniform_int_distribution<int> distr(MIN, MAX);
    return distr(generator);
}

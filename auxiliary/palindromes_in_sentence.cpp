/*
Palindromy w zdaniu
znajduje wszystkie całe wyrazy będące palindromami
*/
#include <iostream>
#include <string>

using namespace std;

bool pali(string);

int main()
{
  string s = "kajak to jest kajak. a, ala to jest ala.";
  short poz, si = s.size();
  string str;
  for (int i=0; i<=s.size(); i++)
  {
    if (s.find(' ') != string::npos)
    {
      poz = s.find(' '); // poszukaj 'space'
      str = s.substr(i,poz); // wytnij wyraz przed 'space'
      s = s.substr(poz+1, s.size());
      if ((str.find(',') != string::npos)) // wytnij przecinek po wyrazie
      {
        poz = str.find(',');
        str = str.substr(i,poz);
      }
      if ((str.find('.') != string::npos)) // wytnij kropkę po wyrazie
      {
        poz = str.find('.');
        str = str.substr(i,poz);
      }
      if (pali(str)) cout << str << endl;
      i = -1;
    }
    else
    {
      if ((s.find('.') != string::npos)) // wytnij końcową kropkę
      {
        poz = s.find('.');
        str = s.substr(i,poz);
        if (pali(str)) cout << str << endl;
      }
      else
        if ((s.find(',') != string::npos)) // wytnij końcowy przecinek
        {
          poz = s.find(',');
          str = s.substr(i,poz);
          if (pali(str)) cout << str << endl;
        }
        else if (pali(s)) cout << s << endl;
        i = s.size();
    }
  }
  return 0;
}

bool pali(string aS)
{
  string str;
  for (short i=aS.size()-1; i>=0; i--)
  {
    str += aS[i];
  }
  if (str == aS) return true;
  else return false;
}

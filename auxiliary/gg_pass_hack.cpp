/*
GG password hack
~ przykładowe złamanie hasła komunikatora gadu-gadu
*/

#include <iostream>
#include <cctype>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
  string s("FECHCHPGCHFD");
  typedef string::const_iterator iter;

  cout << "Hasło gg:\t" << s << endl;
  cout << "Hasło txt:\t";
  for (iter it = s.begin(); it != s.end(); it += 2) {
    /* zamian zakodowanych par liter na ASCII */
    int n = int(*it) - 65; // litera pierwsza (A=0...P=15 co 1)
    int m = (int(*(it+1)) - 65) * 16; // druga (A=0...P=240 co 16)
    cout << char(n + m);
  }
  cout << endl;
  return 0;
}

/*
gg;     FE  CH  CH  PG  CH  FD
text:   E   r   r   o   r   5
ASCII:  69  114 114 111 114 53
np.     F = 5, E = 64; FE = 69
*/

//  iterators example

#include <array>
#include <deque>
#include <forward_list>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using namespace std;

int main() {
    // random access iterator
    vector<int> v;
    deque<int> d;
    array<int, 5> a;
    // examples
    vector<int>::iterator itv = v.begin();
    ++itv;  // faster than postincrement
    itv--;
    itv += 3;
    itv -= 3;
    a[2];
    a.at(2);

    // bidirectional iterator
    list<int> l;
    set<int> s;
    multiset<int> ms;
    map<int, int> m;
    multimap<int, int> mm;
    // examples
    list<int>::iterator itl = l.end();
    --itl;
    itl++;
    //itl -= 3; // ERROR

    // forward iterator
    forward_list<int> fl;
    unordered_map<int, int> um;  // at least
    unordered_set<int> us;       // at least
    // examples
    forward_list<int>::iterator ifl = fl.begin();
    ++ifl;  // olny increment allowed
    //--ifl; // ERROR
    //ifl += 3; // ERROR

    // reverse iterator
    reverse_iterator<vector<int>::iterator> ritv = v.rbegin();  // v.end() - 1
    ritv = v.rend();                                            // v.begin() - 1

    // step
    advance(itv, 3);  // itr += 3
    itv = next(itv, 3);
    itv = prev(itv, 2);

    // distance
    auto dis = distance(v.begin(), v.end());
    dis++;

    // input iterator
    int x = *itv;
    // *itv = 5; // cannot write
    x++;

    // output iterator
    *itv = 5;
    // int x = *itv; // cannot read

    // const interator
    auto citv = std::cbegin(v);
    citv++;

    // insert iterator

    // stream iterator

    return 0;
}
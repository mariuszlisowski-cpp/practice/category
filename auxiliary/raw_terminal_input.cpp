// raw terminal input
// ~ lets input the character without hitting ENTER
// #system

#include <iostream>
#include <unistd.h> // system() method

using std::cin;
using std::cout;
using std::endl;

int main() {
  system("clear"); // clear screen

  char input;
  cout << "Enter any character..." << endl << ": ";

  // reading single character without hitting ENTER
  system("stty raw");
  cin.get(input);
  system("stty cooked"); // restore terminal

  cout << endl << "Entered value: " << input << endl;

  return 0;
}

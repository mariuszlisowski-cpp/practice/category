// occurences of numbers

#include <iostream>

using namespace std;

int main() {
    int arr[] = {9, 6, 5, 6, 1, 7, 2, 4, 3, 5, 7, 7, 9, 6};
    int arrSize = sizeof(arr)/sizeof(*arr);

    int counterSize = 10;
    int * counterArray = new int [counterSize]; // array of 10 elements

    for(int i = 0; i < counterSize; ++i)
        counterArray[i] = 0;

    for(int i = 0; i < arrSize; ++i) {
        ++counterArray[arr[i]]; // increments a value if encounters a number
    }

    for(int i = 0; i < counterSize; ++i)
        cout << counterArray[i] << " occurences of " << i << endl;

    return 0;
}

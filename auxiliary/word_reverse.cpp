/*
Word reverse
#comma_operator #for_loop
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
  string s;
  cout << "Słowo do odwrócenia: ";
  cin >> s;

  char temp;
  int i, j;
  for (j = 0, i = s.size() - 1; j < i; --i, ++j) {
    temp = s[i]; // ostatnia litera do bufora
    s[i] = s[j]; // zastąp ostatnia literę pierwszą
    s[j] = temp; // pierwsza litera z bufora
  }
  cout << "Słowo odwrócone: " << s << endl;
  return 0;
}

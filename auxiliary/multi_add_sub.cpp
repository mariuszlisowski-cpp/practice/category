/*
Multiple, add & substract
~ oblicza wyrażenie w postaci 'a+b-c*d...' lub , 'a + b - c * d =', gdzie znaki arytmetyczne dodawania, odejmowania i mnożenia występują w dowolnych miejscach, a działanie może być zakończone znakiem równości
*/

#include <iostream>
#include <vector>
#include <list>
#include <cctype>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::stoul;
using std::vector;
using std::list;
using std::isdigit;
using std::advance;

void separateNumSign(string, list<unsigned long> &, vector<char> &);
void multipleNumbers(list<unsigned long> &, vector<char> &);
long long addNumbers(list<unsigned long> &, vector<char> &);
void showNumbers(list<unsigned long>);
void showSigns(vector<char>);

int main() {
  string  s = "12 + 8 - 10 * 2 + 98 - 8 ="; // wynik 90
  string s1 = "10-5*5+30"; // wynik 15

  list<unsigned long> ldi;
  vector<char> vch;

  int t;
  cout << "Liczba obliczeń?" << endl << ": ";
  cin >> t;
  cin.ignore();
  while (t--) {
    cout << "Jakie działanie?" << endl << ": ";
    getline(cin, s);

    separateNumSign(s, ldi, vch);
    //showNumbers(ldi);
    //showSigns(vch);
    multipleNumbers(ldi, vch);

    cout << "Wynik działania : "
         << addNumbers(ldi, vch) << endl;

    ldi.clear();
    vch.clear();
  }

  return 0;
}

// rozdziel liczby i znaki arytmetyczne
void separateNumSign(string s, list<unsigned long> &l, vector<char> &v) {
  int i = 0;
  string ss = "";
  unsigned long ul;
  while ((s[i] != '=') && (i < s.length())) {
    if (isdigit(s[i])) {
      ss += s[i]; // lancuch cyfr
    }
    if (isdigit(s[i]) && !isdigit(s[i+1])) {
      ul = stoul(ss); // zamiana lancucha cyfr na liczbe
      l.push_back(ul);
      ss = "";
    }
    if (!isdigit(s[i]) && !isspace(s[i])) {
      v.push_back(s[i]); // znaki arytmetyczne
    }
    ++i;
  }
}

// najpierw mnozenie
void multipleNumbers(list<unsigned long> &l, vector<char> &v) {
  vector<char>::iterator itc;
  list<unsigned long>::iterator it = l.begin();
  for (itc = v.begin(); itc != v.end(); itc++) {
    if (*itc == '*') {
      *(next(it, 1)) = (*it) * (*(next(it, 1)));
      it = l.erase(it); // usuniecie mnoznej
    }
    else
      it++;
  }
}

// potem dodawanie i odejmowanie
long long addNumbers(list<unsigned long> &l, vector<char> &v) {
  vector<char>::iterator itc;
  list<unsigned long>::iterator it = l.begin();
  long long sum = *l.begin();
  for (itc = v.begin(); itc != v.end(); itc++) {
    if (*itc == '+') {
      sum += *(next(it, 1));
      it++;
    }
    if (*itc == '-') {
      sum -= *(next(it, 1));
      it++;
    }
  }
  return sum;
}

// pokaz kontener liczb
void showNumbers(list<unsigned long> l) {
  list<unsigned long>::const_iterator it;
  for (it = l.begin(); it != l.end(); it++) {
    cout << *it << " ";
  }
  cout << endl;
}

// pokaz kontener znakow arytmetycznych
void showSigns(vector<char> v) {
  vector<char>::const_iterator itc;
  for (itc = v.begin(); itc != v.end(); itc++) {
    cout << *itc << " ";
  }
  cout << endl;
}

#include <algorithm>
#include <iostream>
#include <iterator>         // std::inserter()
#include <unordered_map>

int main() {
    std::unordered_map<char, int> alphabet;

    std::generate_n(std::inserter(alphabet, alphabet.begin()), 26,
                  [](){
                      static char c{'a'};
                      auto pair = std::make_pair(c, static_cast<int>(c));
                      ++c;
                      return pair;
                  });

    for (const auto& pair : alphabet) {
        std::cout << pair.first << ", ASCII " << pair.second << std::endl;
    }

    return 0;
}

/*
Pair data type
#pair #make_pair
*/

#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::pair;
using std::make_pair;

int main()
{
  pair<int, int> pa;

  pa = make_pair(1, 2); // bez konwersji
  cout << pa.first << " | " << pa.second << endl;

  pa = make_pair(1.5, 'a'); // konwersja typów na zadeklarowane int
  cout << pa.first << " | " << pa.second << endl;

  pair<float, char> pr;
  pr = make_pair(1.5, 'a'); // już bez konwersji (typy zgodne z deklaracją)
  cout << pr.first << " | " << pr.second << endl;

  auto pi = make_pair(3.14, "raisin"); // (compile with -std=c++11)
  cout << pi.first << " | " << pi.second << endl;

  return 0;
}

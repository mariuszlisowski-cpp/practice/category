/*
cat - file to console print
*/

#include <iostream>
#include <fstream>

using std::cout;
using std::endl;
using std::string;
using std::ifstream;
using std::cerr; // error output (not buffered)

void usage(int);

int main(int argc, char** argv) {
  // only one argument
  if (argc == 2) {
    ifstream file(argv[1]);

    if (file) {
      string s;
      while (getline(file, s))
        cout << s << endl;
    }
    else {
      cerr << "Error opening file!" << endl;
    }
  }
  // no arguments
  else if (argc == 1) {
    usage(1);
  }
  // two or more arguments
  else {
    cout << "Too many arguments!" << endl;
    usage(2);
  }

  return 0;
}

void usage(int arg) {
  string sa("Program prints a text file to console");
  string sb("USAGE: ./cat filename");

  if (arg == 1)
    cout << sa << endl << sb << endl;
  else
    cout << sb << endl;
}

// for_each example

#include <iostream>
#include <algorithm>

using namespace std;

void changeSign(int num) {
    num = -num;
    cout << num << endl;
}

int main() {
    int arr[] {1, -2, 3, -4};
    for_each(arr, arr + 4, changeSign);

    return 0;
}

cmake_minimum_required(VERSION 2.8.11)

project(string_to_integer)

set(GTEST_DIR /Users/Raisin/Develop/repository/gitHub/googletest/)
ADD_SUBDIRECTORY(${GTEST_DIR} gtest)

add_executable(${PROJECT_NAME}-ut ${PROJECT_NAME}-ut.cpp)
target_link_libraries(${PROJECT_NAME}-ut gtest_main)

enable_testing()
add_test(NAME ${PROJECT_NAME} COMMAND ${PROJECT_NAME}-ut)

#include <iostream>
#include <string>
#include <variant>

struct Visitor {
    void operator()(int value) const { std::cout << "int " << value << '\n'; }
    void operator()(double value) const { std::cout << "double " << value << '\n'; }
    void operator()(bool value) const { std::cout << "bool " << value << '\n'; }
    void operator()(std::string& value) const { std::cout << "string " << value << '\n'; }
};

int main() {
    std::variant<int, double, bool, std::string> var = std::string("variant");
    std::visit(Visitor{}, var);

    return 0;
}

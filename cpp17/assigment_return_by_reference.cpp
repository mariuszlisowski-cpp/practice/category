/* Operators that have the same precedence are bound to their arguments in the direction of their associativity.
   For example, the expression a = b = c is parsed as a = (b = c), and not as (a = b) = c
   because of right-to-left associativity of assignment, but a + b - c is parsed (a + b) - c
   and not a + (b - c) because of left-to-rightassociativity of addition and subtraction.
*/
#include <iostream>

class Clazz {
public:
    Clazz() {}
    Clazz(int val) : val_(val) {}

    // defined return by reference
    Clazz& operator=(const Clazz& other) {
        val_ = other.val_;

        return *this;
    }

    friend std::ostream& operator<<(std::ostream& os, const Clazz&);

private:
    int val_;
};

std::ostream& operator<<(std::ostream& os, const Clazz& obj) {
    os << obj.val_;

    return os;
}

int main() {
    // primitive data type (buil in return by reference)
    int a{ 9 }, b, c{ 2 };
    (b = c) = a;                                        // assigning c to b then a to b
                                                        // b becomes 2 then b becomes 9
    std::cout << "> a:  " << a;
    std::cout << ", b:  " << b;                         // OK
    std::cout << ", c:  " << c << std::endl;

    // user data type (defined return by reference)
    Clazz objA(99);
    Clazz objB, objC;                                   // non initialized (garbage)

    (objB = objC) = objA;                               // assigning C to B then A to B
                                                        // B becomes C (garbage) then B becomes A (99)
    std::cout << "> A: " << objA;                       // OK
    std::cout << ", B: " << objB;                       // OK
    std::cout << ", C: " << objC << std::endl;          // still garbage

    
    objB = (objC = objA);                               // assigning A to C then C to B then A to B
                                                        // C becomes A (99) then B becomes A (99)
    std::cout << "> A: " << objA;                       // OK
    std::cout << ", B: " << objB;                       // OK
    std::cout << ", C: " << objC << std::endl;          // OK
    
    
    objB = objC = objA;                                 // parsed as A = B = C not (A = B) = C (as above)
                                                        // because of right-to-left associativity of assignment
    std::cout << "> A: " << objA;                       // OK
    std::cout << ", B: " << objB;                       // OK
    std::cout << ", C: " << objC << std::endl;          // OK

    return 0;
}

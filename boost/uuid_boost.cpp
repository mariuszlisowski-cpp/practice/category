#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>         // streaming operators etc.
#include <iostream>
#include <map>
#include <string>

class URL {
public:
    URL(std::string url) : url_(url) {}

    std::string get_url() {
        return url_;
    }
private:
    std::string url_;
};

int main() {
    std::map<std::string, URL> urls_history;
        
    // browse
    boost::uuids::uuid uuid = boost::uuids::random_generator()();
    urls_history.insert({boost::uuids::to_string(uuid), URL("https://stackoverflow.com")});


    for (auto& [uuid, url] : urls_history) {
        std::cout << uuid << " : " << url.get_url() << std::endl;
    }

    return 0;
}

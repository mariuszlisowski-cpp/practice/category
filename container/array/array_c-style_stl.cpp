// array c-style vs stl

#include <array>
#include <iostream>

template <class T>
void consoleStreamArray(T array) {
    for (auto it = std::begin(array); it != std::end(array); ++it) {
        std::cout << *it << ' ';
    }
}
int main() {
    // C-style array
    int arr1[]{ 1, 2, 3, 4, 5 };
    
    // C++ array (wrapper)
    std::array<int, 5> arr2{ 1, 2, 3, 4, 5 };

    return 0;
}
// passing 2D array by pointer

#include <iostream>

using namespace std;

void showArray(int*, int, int);

int main() {
  int arr[6][7] = {
    { 1, 1, 1, 0, 0, 0, 1 },
    { 0, 1, 0, 0, 0, 0, 2 },
    { 1, 1, 1, 0, 0, 0, 3 },
    { 0, 0, 2, 4, 4, 0, 4 },
    { 0, 0, 0, 2, 0, 0, 5 },
    { 0, 0, 1, 2, 4, 0, 6 }
  };

  int rows = sizeof(arr) / sizeof(arr[0]);
  int cols = sizeof(arr[0]) / sizeof(*arr[0]);

  showArray((int *)arr, rows, cols);

  return 0;
}

void showArray(int* arr, int rows, int cols) {
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      cout << *((arr+i*cols) + j) << " ";
    }
    cout << endl;
  }
}

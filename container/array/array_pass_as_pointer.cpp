#include <iostream>

// array decay to a pointer
void fooA(char* array, size_t len) {
    for (size_t i = 0; i < len; i++) {
        std::cout << array[i] << ' ';
    }
}

// array decay to a pointer
void fooB(char array[], size_t len) {
    for (size_t i = 0; i < len; i++) {
        std::cout << array[i] <<  ' ';
    }
}

int main() {
    char arr[5] = {'A', 'B', 'C', 'D', 'E'};
    size_t size = sizeof(arr) / sizeof(*arr);
    fooA(arr, size);  // explicit length passed
    fooB(arr, size);  // explicit length passed
    
    return 0;
}

/*
Char array duplicate
*/

#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::copy;
using std::string;

char* duplicate(const char*);

int main() {

  char word[7] = {"raisin"}; // '\0' automaticlly added
  char text[] = {"example"}; // size 8
  char* dup = duplicate(word);
  char* cpy = duplicate(text);

  cout << dup << endl;
  cout << cpy << endl;

  delete dup; // created in function as r pointer
  delete cpy; // s/a

  return 0;
}

char* duplicate(const char* p) {
  size_t len = strlen(p) + 1; // '+1' because of '\0' at the end
  char* r = new char[len];

  copy(p, p+len, r);

  return r;
}

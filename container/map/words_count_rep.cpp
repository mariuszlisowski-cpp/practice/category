/*
Zliczanie powtórzonych wyrazów
wyświetla w którym wierszu powtórzył się wyraz
*/

#include <iostream>
#include <map>
#include <vector>
#include <cctype>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::map;
using std::vector;
using std::isspace;
using std::istream;

vector<string> split(const string& s);
map<string, vector<int> > xref(istream& in, vector<string> find_words(const string&) = split);

int main()
{
  map<string, vector<int> > ret = xref(cin);

  typedef map<string, vector<int> >::const_iterator iter;

  for (iter it = ret.begin(); it != ret.end(); it++) {
      cout << "wyraz \"" << it -> first << "\" wystąpił w wierszu: "; // wypisz wyraz

      vector<int>::const_iterator line_it = it -> second.begin();
      cout << *line_it; // wypisz pierwszy numer wiersza

      ++line_it;
      while (line_it != it -> second.end()) {
        cout << ", " << *line_it; // reszta numerów wierszy
        ++line_it;
      }
    cout << endl;
  }
  return 0;
}

/*  generowanie odsyłaczy */
map<string, vector<int> > xref(istream& in, vector<string> find_words(const string&)) {
  string line;
  int line_number = 0;
  map<string, vector<int> > ret;
  cout << "Wprowadź kilka zdań (zakończ CTRL-D)..." << endl << "1: ";
  while (getline(in, line)) {
    ++line_number;
    cout << line_number+1 << ": ";
    vector<string> words = find_words(line);
    for (vector<string>::const_iterator it = words.begin(); it != words.end(); ++it) {
      ret[*it].push_back(line_number);
    }
  }
  cout << endl;
  return ret;
}

/* dzieli ciąg na wyrazy i zwraca w postaci kontenera */
vector<string> split(const string& s) {
    vector<string> ret;
    typedef string::size_type string_size;
    string_size i = 0;

    // niezmiennik: przetworzyliśmy [wartość początkowa i, i)
    while (i != s.size()) {
        // pomiń ewentualne początkowe odstępy
        // niezmiennik: znaki w zakresie [początkowe i, bieżące i) to znaki odstępu
        while (i != s.size() && isspace(s[i]))
            ++i;

        // wyszukaj następny wyraz
        string_size j = i;
        // niezmiennik: żaden ze znaków w zakresie [i, j) nie jest znakiem odstępu
        while (j != s.size() && !isspace(s[j]))
            ++j;

        // jeżeli znaleziono jakieś znaki nie będące odstępami
        if (i != j) {
            // kopiuj z ciągu s od znaku i j - i znaków
            ret.push_back(s.substr(i, j - i));
            i = j;
        }
    }
    return ret;
}

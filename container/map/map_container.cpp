/*
Kontener asocjacyjny
(tutaj: składa się z pary 'string' i elementów wektora)
*/

#include <iostream>
#include <string>
#include <vector>
#include <map>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::map;
using std::vector;

vector<string> fillVector();

int main()
{
  string s;
  int i = 0, t = 1;
  map<string, vector<string> > m; // kontener asocjacyjny
  vector<string> v; // kontener

  /* wprowadzanie do kontenera asocjacyjnego */
  while (true) {
    cout << "Nazwa mapy... (ENTER kończy)" << endl << ": ";
    getline(cin, s);
    if (s.length() == 0)
      break;
    v = fillVector();
    for (vector<string>::const_iterator it = v.begin(); it != v.end(); ++it) {
      m[s].push_back(*it);
    }
    ++i;
  }

  /*  wyświetlanie kontenera 'map' */
  cout << endl << "Wprowadzone elementy mapy..." << endl;
  for (map<string, vector<string> >::const_iterator it = m.begin(); it != m.end(); it++) {
      vector<string>::const_iterator itt;
      cout << it -> first << "\t : ";
      for (itt = it -> second.begin(); itt != it -> second.end(); ++itt) {
        cout << *itt << " ";
      }
      cout << endl;
  }

  return 0;
}

/* wprowadzanie elementów wektora */
vector<string> fillVector() {
  vector<string> retV;
  cout << "Elementy mapy... (ENTER kończy)..." << endl;
  string s;
  while (true) {
    cout << ": ";
    getline(cin, s);
    if (s.length() == 0)
      break;
    retV.push_back(s);
  }
  cout << endl;
  return retV;
}

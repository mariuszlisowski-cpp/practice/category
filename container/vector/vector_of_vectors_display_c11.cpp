// vector of vectors array display

#include <iostream>
#include <vector>

using namespace std;

void showArray(const vector<vector<int> >);
void showArrayIter(const vector<vector<int> >);

int main() {

  vector<vector<int>> arr {
    { 1, 1, 1, 0, 0, 0 },
    { 0, 1, 0, 0, 0, 0 },
    { 1, 1, 1, 0, 0, 0 },
    { 0, 0, 2, 4, 4, 0 },
    { 0, 0, 0, 2, 0, 0 },
    { 0, 0, 1, 2, 4, 0 }
  };

  showArray(arr);
  cout << endl;
  showArrayIter(arr);

  return 0;
}

void showArray(const vector<vector<int> > arr) {
  for (auto row : arr) {
     for (auto col : row)
        cout << col << " ";
     cout << endl;
  }
}

void showArrayIter(const vector<vector<int> > arr) {
  vector<vector<int> >::const_iterator row;
  vector<int>::const_iterator col;
  for (row = arr.begin(); row != arr.end(); row++) {

    for (col = row->begin(); col != row->end(); col++) {
      cout << *col << " ";

    }
    cout << endl;
  }
}

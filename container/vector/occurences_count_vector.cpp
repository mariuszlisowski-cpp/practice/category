// occurences count in array
// #count

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <set>

using namespace std;

vector<int> matchingStrings(vector<string>, vector<string>);
template <class T>
void showArray(vector<T>);

int main() {
   cout << "First array..." << endl;
   vector<string> inputs { "aa", "aa", "bb", "cc", "ccc", "aa" };
   showArray(inputs);

   cout << "Second array..." << endl;
   vector<string> queries { "aa", "cc", "ab" };
   showArray(queries);

   cout << endl << "Number of occurences:" << endl;
   vector<int> occurences = matchingStrings(inputs, queries);
   showArray(occurences);

   return 0;
}

// returns the number of occurences of each element of the second array
vector<int> matchingStrings(vector<string> strings, vector<string> queries) {
   vector<int> results;
   for (auto it : queries) {
      results.push_back(count(strings.begin(), strings.end(), it));
   }
   return results;
}

// displays arrays
template <class T>
void showArray(vector<T> vec) {
   for (auto x : vec)
      cout << x << " ";
   cout << endl;
}

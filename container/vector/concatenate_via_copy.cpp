#include <iostream>
#include <vector>

/* helper function */
template<typename T>
std::vector<T> concatenate(const std::vector<T>& lhs, const std::vector<T>& rhs){
    auto result = lhs;
    std::copy(rhs.begin(), rhs.end(), std::inserter(result, result.end()));

    return result;
}

int main() {
    std::vector<int> odd = { 1, 2, 3 };
    std::vector<int> even = { 4, 5, 6 };

    auto concatenated{ concatenate(odd, even) };

    for (const auto& el : concatenated) {
        std::cout << el << ' ';
    }

    return 0;
}

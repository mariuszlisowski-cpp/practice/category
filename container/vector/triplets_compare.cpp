/*
Triplets compare
*/

#include <iostream>
#include <vector>

using namespace std;

vector<int> compareTriplets(vector<int>, vector<int>);

int main() {
  vector<int> v{ 1, 2, 3}; // C++11 list initialization
  vector<int> w{ 3, 2, 1};
  vector<int> result = compareTriplets(v, w);
  cout << result[0] << " "<< result[1] << endl;
  v[1]++;
  result = compareTriplets(v, w);
  cout << result[0] << " "<< result[1] << endl;

  return 0;
}

vector<int> compareTriplets(vector<int> a, vector<int> b) {
    static vector<int> res(2, 0);
    vector<int>::const_iterator ita = a.begin();
    vector<int>::const_iterator itb = b.begin();
    for (;ita != a.end(); ita++, itb++ ) {

        if (*ita > *itb) res[0]++;
        if (*ita < *itb) res[1]++;
    }
    return res;
}

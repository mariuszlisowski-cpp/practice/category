// missing element
// ~ finds a missing element within a given array of 1..N distinct elements
// ~ array consists of 1..N+1 elements (one element is missig then)
// ~ examples: empty array - missing 1 | one element (1) - missing 2
// time complexity O(n)
// #XOR_method

#include <iostream>
#include <vector>

using namespace std;

int missingElement(vector<int> &);
void displayArray(const vector<int>&);

int main() {
  vector<int> vec { 1, 2, 3, 4, 5, 6, 8, 9 }; // 7 missing

  cout << "Finds missing element in an array..." << endl;
  int miss = missingElement(vec);
  cout << "Missing element: " << miss << endl;

  return 0;
}
// [a XOR a = 0] [a XOR 0 = a]
int missingElement(vector<int> &A) {
  int missing;
  if (A.size() >= 1) {
    int xorComplete = 1;
    for (unsigned int i = 2; i <= A.size() + 1; i++) {
      xorComplete ^= i; // XOR all range elements (1..N+1)
    }
    cout << "~ complete table XOR-ed:   " << xorComplete << endl;

    int xorIncomplete = A.at(0);
    vector<int>::const_iterator it;
    for (it = A.begin() + 1; it != A.end(); it++) {
      xorIncomplete ^= *it; // XOR all array elements (1..N)
    }
    cout << "~ incomplete table XOR-ed: " << xorIncomplete << endl;

    missing = xorComplete ^ xorIncomplete;
    cout << "  " << xorComplete << " XOR " << xorIncomplete << " = " << missing << endl;
  }
  else // if (A.size() == 0)
    missing = 1;

  return missing;
}

void displayArray(const vector<int>& arr) {
  vector<int>::const_iterator it;
  for (it = arr.begin(); it != arr.end(); it++)
    cout << *it << " ";
  cout << endl;
}

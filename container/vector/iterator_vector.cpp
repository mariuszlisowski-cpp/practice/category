/*
Iterator - vector
*/

#include <iostream>
#include <vector>
#include <iterator>
#include <ctime>

using namespace std;

/* deklaracja funkcji */
void vectorDisplayForward(vector<int>);
void vectorDisplayBackward(vector<int>);

/* deklaracja stałych */
const short col = 10; // ilość kolumn przy wyświetlaniu
const short divider = 150; // miejsce podziału kontenera
const short cap = 100; // pojemność kontenera pierwotnego
const short minn = 100, maxx = 200; // zakres liczb losowych

int main()
{

  /* inicjalizacja generatora liczb pseudolosowych */
  srand(time(0));

  /* zapełnienie wektora losowymi liczbami */
  vector<int> v; // wektor zawierający zmienne typu 'int'
  int losowa;
  short t = 0;
  while (t < cap)
  {
    losowa = rand()%(maxx-minn+1)+minn;
    v.push_back(losowa); // losowe liczby do wektora
    t++;
  }

  /* wyświetlenie wektora 'v' */
  cout << "Wartości losowe kontenera..." << endl;
  vectorDisplayForward(v);
  cout << endl;

  /* skopiowanie z wektora 'v' elemenów większych niż... do wektora 'vec' */
  vector<int> vec;
  vector<int>::const_iterator iter = v.begin(); // deklaracja iteratora
  while (iter != v.end())
  {
    if (*iter > divider)
    {
      vec.push_back(*iter);
      iter = v.erase(iter); // zwraca iterator za elementem usuwanym
    }
    else ++iter;
  }

  /* wyświetlenie wektora 'vec' */
  cout << "Wartości większe od " << divider << "..." << endl;
  vectorDisplayForward(vec);
  cout << endl;
  cout << "A posortowane rosnąco..." << endl;
  sort(vec.begin(), vec.end());
  vectorDisplayForward(vec);
  cout << endl << endl;


  /* wyświetlenie wektora 'v' po modyfikacji */
  cout << "Wartości mniejsze lub równe " << divider << "..." << endl;
  vectorDisplayForward(v);
  cout << endl;
  cout << "A posortowane malejąco..." << endl;
  sort(v.begin(), v.end());
  vectorDisplayBackward(v);
  cout << endl << endl;

  return 0;
}

/* wyświetlenie wektora rosnąco (z formatowaniem na kolumny) */
void vectorDisplayForward(vector<int> aV)
{
  vector<int>::const_iterator iter = aV.begin(); // deklaracja iteratora
  while (iter != aV.end())
  {
    cout << *iter << " "; // dereferencja iteratora podaje wartość wektora
    if ((iter-aV.begin()+1)%col == 0) cout << endl; // formatowanie w kolumny
    ++iter;
  }
}

/* wyświetlenie wektora malejąco (z formatowaniem na kolumny) */
void vectorDisplayBackward(vector<int> aV)
{
  vector<int>::const_iterator iter = aV.end(); // deklaracja iteratora
  while (iter != aV.begin())
  {
    cout << *iter << " "; // dereferencja iteratora podaje wartość wektora
    if ((iter-aV.end()-1)%col == 0) cout << endl; // formatowanie w kolumny
    --iter;
  }
}

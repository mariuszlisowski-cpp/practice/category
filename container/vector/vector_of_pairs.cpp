/*
Vector of pairs
#vector #pair #make_pair
*/

#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::pair;
using std::make_pair;
using std::vector;

int main()
{
  const int a = 4; // a tablic...
  const int b = 2; // ...po b elementów
  int arr[a][b] = {  { 1, 2 },
                     { 3, 4 },
                     { 5, 6 },
                     { 7, 8 },
                  };
  vector<pair<int, int> > vec; // wektor to tablica (tutaj: par)

  for (int i = 0; i < a; i++) {
    vec.push_back( make_pair(arr[i][0], arr[i][1]) );
    cout << vec[i].first << " | " << vec[i].second << endl;
  }

  return 0;
}

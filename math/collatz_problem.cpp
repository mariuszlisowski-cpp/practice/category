#include <iostream>
#include <list>

using namespace std;

int collatz(unsigned int c) {
  return (c % 2 == 0) ? c/2 : (3 * c) + 1;
}

int main() {
  unsigned long int c;
  list<unsigned int> data;
  cout << "Wpisz liczbe c:\n";
  cin >> c;
  while (c != 1) {
    c = collatz(c);
    data.push_back(c);
  }
  for (unsigned int i : data) {
    cout << i << '\n';
  }

  return 0;
}

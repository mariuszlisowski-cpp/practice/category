#include <iostream>

// 5)  6 * fac(5) = 6 * 120 = 720
// 4)  5 * fac(4) = 5 * 24 = 120
// 3)  4 * fac(3) = 4 * 6 = 24
// 2)  3 * fac(2) = 3 * 2 = 6
// 1)  2 * fac(1) = 2 * 1 = 2
int factorialRecurA(int n) {
    if (n < 2) {
        return 1;
    }
 
    return n * factorialRecurA(n - 1);
}

// 6 * fact(5) * fact(4) * fact(3) * fact(2)
// 6 *      5  *      4  *      3  *      2
unsigned long long factorialRecurB(size_t n) {
    static unsigned long long result{1};

    if (n > 1) {
        result *= n;
        factorialRecurB(n - 1);
    }

    return result;
}

// 1 is ignored
// 2 * 3 * 4 * 5 * 6
unsigned long long factorial(size_t n) {
    unsigned long long result{1};
    for (size_t i = 2; i <= n; ++i) {
        result *= i;
    }

    return result;
}

int main() {
    std::cout << factorial(6) << std::endl;
    std::cout << factorialRecurA(6) << std::endl;
    std::cout << factorialRecurB(6) << std::endl;

    return 0;
}

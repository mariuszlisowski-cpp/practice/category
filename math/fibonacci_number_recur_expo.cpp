/* exponential time complexity */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

// fibo:     0 1 1 2 3 5 8 13 21 34 55 89 144 ...
// index:    0 1 2 3 4 5 6  7  8  9 10 11  12
int fibonacci(int n) {
    if (n <= 1) {
        return n;
    }
 
    return fibonacci(n - 1) + fibonacci(n - 2);
}

int main() {
    for (int i = 0; i <= 8; ++i) {                                  // index [0, 8]
        std::cout << fibonacci(i) << ' ';                           // 0 1 2 3 5 8 13 21
    }

    return 0;
}

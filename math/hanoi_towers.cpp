#include <iostream>
#include <time.h>

using namespace std;

void hanoi(int n, char A, char B, char C)
{
  // przekłada n krążków z A korzystając z B na C
  if (n > 0)
  {
    hanoi(n-1, A, C, B);
    // cout << A << " -> " << C << endl;
    hanoi(n-1, B, A, C);
  }
}

int main(int argc, char *argv[])
{
  int n;
  cout << "\nWieże Hanoi...";
  cout << "\nPodaj liczbę krążków do przełożenia: ";
  cin >> n;
  cout << "\nObliczam...\n";

  clock_t start, stop;
  double czas;

  start=clock();
  hanoi(n, 'A', 'B', 'C');
  stop=clock();

  czas=(double)(stop-start)/CLOCKS_PER_SEC;
  cout << "\nCzas przełożenia wieży wyniósł: " << czas << " s." << endl << endl;

  return 0;
}

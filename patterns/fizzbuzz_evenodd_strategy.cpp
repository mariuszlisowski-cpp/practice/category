#include <iostream>
#include <numeric>
#include <vector>

/* strategy pattern */
class Algo {
public:
    virtual ~Algo() = default;

    void operator()(const std::vector<int>& vec) const {
        size_t sum{};
        for (const auto el : vec) {
            if (predA(el) && predB(el)) {
                printA(el);
                printB(el);
                printEOL();
                continue;
            }
            if (predA(el)) {
                printA(el);
                printEOL();
                continue;
            }
            if (predB(el)) {
                printB(el);
                printEOL();
            }
        }
    }

    virtual bool predA(int) const = 0;
    virtual bool predB(int) const = 0;
    virtual void printA(int) const = 0;
    virtual void printB(int) const = 0;

    void printEOL() const {
        std::cout << std::endl;
    };
};

/* 1st strategy */
class FizzBuzz : public Algo {
public:
    ~FizzBuzz() override = default;

    bool predA(int number) const override {
        return number % 3 == 0;
    }
    bool predB(int number) const override {
        return number % 5 == 0;
    }

    void printA(int el) const override {
        std::cout << a_ << ": " << el;
    }
    void printB(int el) const override {
        std::cout << b_ <<  ": " << el;
    }

private:
    std::string a_{" fizz"};
    std::string b_{" buzz"};
};

/* 2nd strategy */
class EvenOdd : public Algo {
public:
    ~EvenOdd() override = default;

    bool predA(int number) const override {
        return number & 1;
    }
    bool predB(int number) const override {
        return !(number  & 1);
    }

    void printA(int el) const override {
        std::cout << a_ << ": " << el;
    }
    void printB(int el) const override {
        std::cout << b_ <<  ": " << el;
    }

private:
    std::string a_{"odd"};
    std::string b_{"even"};
};


int main() {
    /* strategy choice */
    const Algo& algo = FizzBuzz();                                      // 1st strategy
    // const Algo& algo = EvenOdd();                                    // 2nd strategy

    std::vector<int> v(50);
    std::iota(begin(v), end(v), 1);

    algo(v);



    return 0;
}

#pragma once

#include <memory>
#include <vector>

class Customer;
class CustomerList;
using CustomerPtr = std::shared_ptr<Customer>;

using CustomerListPtr = std::shared_ptr<CustomerList>;

class IsEqualTo final {
public:
    explicit IsEqualTo(const CustomerPtr& customer);
    bool operator()(const CustomerPtr& customerToCompare);

private:
    CustomerPtr customer_;
};

class CustomerService {
public:
    void addCustomerToRegister(const CustomerPtr& customerToAdd);
    CustomerList findCustomerBySurname(const std::string& surname);
    void displayCustomerRegister();

protected:
    std::vector<CustomerPtr> customers_;
};

#include "customer.hpp"
#include "customerList.hpp"

#include <iostream>

CustomerList::~CustomerList() {}

void CustomerList::addFoundCustomerToFoundList(const CustomerPtr& customer) {
    std::cout << "...adding customer" << std::endl;
    // FIXME:
    customerFoundList_.push_back(customer);
}

void CustomerList::displayFoundList() {
    std::cout << "\nList of found customers:" << std::endl;
    // TODO:
    std::cout << "...size: " << customerFoundList_.size() << std::endl;
    for (const auto& customer : customerFoundList_) {
        std::cout << customer->getSurname() << ", " << customer->getForename() << std::endl;
    }
}

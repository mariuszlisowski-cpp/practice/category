#pragma once

#include <algorithm>
#include <vector>

#include "observer.hpp"

class IsEqualTo final {
public:
    explicit IsEqualTo(const ObserverPtr& observer)
        : observer{ observer } {}
    bool operator()(const ObserverPtr& observerToCompare) {
        return observerToCompare->getID() == observer->getID();
    }

private:
    ObserverPtr observer;
};

class Subject {
public:
    void addObserver(ObserverPtr& observerToAdd) {
        auto iter = std::find_if(std::begin(observers),
                                 std::end(observers),
                                 IsEqualTo(observerToAdd));
        if (iter == std::end(observers)) {
            observers.push_back(observerToAdd);
        }
    }

    void removeObserver(ObserverPtr& observerToRemove) {
        observers.erase(std::remove_if(std::begin(observers),
                                       std::end(observers),
                                       IsEqualTo(observerToRemove)),
                        std::end(observers));
    }

    void showObservers() {
        std::cout << std::string(26, '-') << std::endl;
        for (const auto& observer : observers) {
            std::cout << "Observer ID: " << observer->getID() << std::endl;
        }
    }

protected:
    void notifyAllObservers() const {
        for (const auto& observer : observers) {
            observer->update();
        }
    }

private:
    std::vector<ObserverPtr> observers;
};

// 

#include "spreadsheetModel.hpp"
#include "spreadsheetViews.hpp"

int main() {
    SpreadsheetModel spreadsheetModel{};

    ObserverPtr observer1 = std::make_shared<TableView>(spreadsheetModel);
    spreadsheetModel.addObserver(observer1);

    ObserverPtr observer2 = std::make_shared<BarChartView>(spreadsheetModel);
    spreadsheetModel.addObserver(observer2);

    ObserverPtr observer3 = std::make_shared<PieChartView>(spreadsheetModel);
    spreadsheetModel.addObserver(observer3);

    spreadsheetModel.showObservers();
    spreadsheetModel.changeCellValue("A", 5, 3.14);

    // -----------------------------------------------------------------------
    spreadsheetModel.removeObserver(observer1);
    spreadsheetModel.showObservers();
    spreadsheetModel.changeCellValue("A", 6, 3.14);

    // -----------------------------------------------------------------------
    spreadsheetModel.removeObserver(observer2);
    spreadsheetModel.showObservers();
    spreadsheetModel.changeCellValue("A", 7, 3.14);

    // -----------------------------------------------------------------------
    spreadsheetModel.removeObserver(observer3);

    return 0;
}
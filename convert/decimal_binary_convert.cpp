// decimal binary convert

#include <iostream>

using std::cout;
using std::endl;
using std::string;

string toBinary(int n) {
    string str;
    do {
        str = (n % 2 == 0 ? "0" : "1") + str;
    } while ((n /= 2));
    return str;
}

int main() {
  int l{255};

  cout << toBinary(l) << endl;

  return 0;
}

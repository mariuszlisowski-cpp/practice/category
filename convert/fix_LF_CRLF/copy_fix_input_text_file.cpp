// copy & fix input text file
// ~ input converts \r\n to \n
// ~ output converts \n to \r\n
// ~ so it fixes broken line feeds in input text file
// ~ by replacing \n with \r\n

#include <fstream>
#include <iostream>
#include <string>

void printMessageAndExit() {
        std::cout << "USAGE: ./copy <inputFile> <outputFile>" << std::endl;
        exit(EXIT_FAILURE);
}


int main(int argc, char** argv) {
    std::string inputFilename,
                outputFilename;

    if (argc == 1) {
        // no arguments
        std::cout << "Utility to make a copy of a text file" << std::endl;
    }
    else if (argc == 2) {
        // one argument
        std::cerr << "Two arguments needed!" << std::endl;
        printMessageAndExit();
    }
    else if (argc == 3) {
        // two arguments
        inputFilename = static_cast<std::string>(argv[1]);
        outputFilename = static_cast<std::string>(argv[2]);

    } else {
        // three or more arguments
        std::cerr << "Too many arguments!" << std::endl;
        printMessageAndExit();
    }

    std::ifstream inputFile(inputFilename);
    std::ofstream outputFile(outputFilename);
    if (!inputFile || !outputFile) {
        std::cerr << "File opening error!" << std::endl;
        return 1;
    }
    char ch;
    // read until EOF
    while (inputFile.get(ch)) {
        if (!outputFile.put(ch)) {
            std::cerr << "File write error!" << std::endl;
        }
    }
    // if no EOF occured
    if (!inputFile.eof()) {
        std::cerr << "Unknown read error!" << std::endl;
    }

    return 0;
}

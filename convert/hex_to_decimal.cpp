#include <cctype>
#include <cmath>
#include <iostream>
#include <string>
#include <unordered_map>

unsigned getDecimal(char hex) {
    char hex_symbol = std::tolower(hex);
    std::unordered_map<char, unsigned> hex_symbols({
        {'a', 10},
        {'b', 11},
        {'c', 12},
        {'d', 13},
        {'e', 14},
        {'f', 15}
    });
    if (hex_symbol >= '1' && hex_symbol <= '9') {
        return hex_symbol - '0';
    }
    return hex_symbols[hex_symbol];
}

auto hexToDecimal(const std::string& str) {
    unsigned hex_base{16};
    unsigned position{};
    unsigned decimal{};
    for (auto it = str.crbegin(); it != str.crend(); ++it, ++position) {
        std::cout << *it << ' ' << getDecimal(*it) << std::endl;            // verbose
        decimal += getDecimal(*it) * std::pow(hex_base, position);
    }

    return decimal;
}

int main() {
    // std::string str("ABCDEF");
    std::string str("FFFF");

    std::cout << hexToDecimal(str) << std::endl;

    return 0;
}

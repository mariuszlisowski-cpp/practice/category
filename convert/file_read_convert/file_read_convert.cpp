/*
File reading & converting
~ prevents some file reading errors and converts input data to 'double' type
# compile within the source directory to gain access to the file being read
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>

using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::fstream;
using std::stringstream;
using std::ios;
using std::fixed;
using std::setprecision;

int main()
{
  double* decodedFreq = new double [26];
  double* encodedFreq = new double [26];

  // wczytanie zdekodowanej tablicy alfabetu z pliku
  fstream decFreq;
  decFreq.open("data.text", ios::in);
  if (!decFreq.good()) {
    cout << "Błąd otwarcia pliku!" << endl;
    exit(0);
  }
  else {
    string s;
    int i = 0, space;
    while (!decFreq.eof()) {
      getline(decFreq, s);
      space = s.find(0x20);
      s = s.erase(0,space+1);
      if (stringstream(s) >> decodedFreq[i++]) {
        cout << fixed << setprecision(s.size()) << decodedFreq[i-1] << endl;
      }
      else {
        if (!decFreq.eof()) {
          cout << "Błąd w pliku!" << endl;
          break;
        }
      }
    }
    decFreq.close();
  }

  return 0;
}

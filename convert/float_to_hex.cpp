/* Konwersja liczb typu 'float' na ich reprezentację w pamięci (hex) */

#include <iostream>
#include <iomanip> // dla polecenia 'setprecision'

using namespace std;

void floatToHex(float);
void hexToFloat(int);
void hexToFloat(unsigned char*); // przeciążenie funkcji (tutaj: procedury)

int main()
{
  /* przykładowe dane */
  auto f = 1234.567749f; // f-'float'; l-'long' (typ 'auto' od wersji c++11)
  int h = 0x449a522b; // 0x - oznaczenie zapisu hexadecymalnego
  unsigned char ch[4] = {0x2b, 0x52, 0x9a, 0x44}; // adresowanie LE

  /* zamiana float na hex (adresowanie LE, tj. 'Little Endian') */
  floatToHex(f);

  /* zamiana hex na float z użyciem typu 'int' */
  hexToFloat(h);

  /* zamiana hex na float z użyciem typu 'chr' */
  hexToFloat(ch);

  return 0;
}

void floatToHex(float aF)
{
  /*
  wskaźnik do tablicy elementów jednobajtowych przypisany przez rzutowanie adresu do zmiennej typu 'float'
  */
  unsigned char* c = (unsigned char*)&aF;

  /*
  adresowanie LE, tj. 'Little Endian' (od prawej do lewej) z uwzględnieniem indeksów tablicy typu 'char'
  */
  for (int i=sizeof(aF)-1; i>=0; i--)
  {
    /*
    wymuszenie wyświetlania 'hex' z jednoczesnym rzutowaniem na typ 'int' w celu poprawnego wyświetlania wartości hexadecymalnych
    */
    cout << hex << (unsigned int)c[i];
  }
  cout << endl;
}

/*
sposób konwersji: pobranie adresu wartości 'int', zrzutowanie go na wskaźnik do typu 'float' i wyłuskanie z powrotem wartości spod tego wskaźnika (już typu docelowego)
*/
void hexToFloat(int aH)
{
  float f = *(float*)&aH;
  cout << setprecision(6) << fixed << f << endl;
}

/*
przekazanie tablicy typu 'char' jako wskaźnik jej początkowego adresu; sposób konwersji jak powyżej
*/
void hexToFloat(unsigned char* aCh)
{
  float f = *(float*)aCh;
  cout << setprecision(6) << fixed << f << endl;
}

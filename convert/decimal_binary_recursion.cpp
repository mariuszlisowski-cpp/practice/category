// decimal to binary

#include <iostream>

using namespace std;

void showBinary(int decimal) {
    int bit = decimal % 2;
    if (decimal > 1)
        showBinary(decimal / 2);
    cout << bit;
}


int toBinary(int decimal)
{
    if (decimal == 0)
        return 0;
    return (decimal % 2 + 10 * toBinary(decimal / 2));
}


int main() {
    showBinary(14);

    cout << endl << 2 % 4; // int(2/4) = 0; 0 * 4 = 0; 2 - 0 = 2
    cout << endl << 7 % 2; // int(7/2) = 3; 3 * 2 = 6; 7 - 6 = 1
    cout << endl;

    cout << toBinary(14);

    return 0;
}

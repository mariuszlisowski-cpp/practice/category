#include <iostream>
#include <ranges>

int main() {
    constexpr auto RANGE{2};

    for (auto&& _ : std::ranges::iota_view{0, RANGE}) {                       // _ valid variable character (unused)
        std::cout << "repeat iota_view..." << std::endl;
    }

    for (auto&& _ : std::views::iota(0, RANGE)) {
        std::cout << "repeat iota..." << std::endl;
    }

    return 0;
}

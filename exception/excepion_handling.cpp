// excepion handling
// #try #catch #stoi

#include <iostream>

using namespace std;

int main() {
  string s;
  cout << "Enter the string to be converted..." << endl << ": ";
  cin >> s;
  try {
    cout << stoi(s) << endl;
  }
  catch (const invalid_argument& ia) {
    cerr << "Invalid argument: " << ia.what() << endl;
  }

  return 0;
}

// excepion throw
// #try # catch # exception #throw #domain_error #what

#include <cmath>
#include <iostream>
#include <exception>
#include <stdexcept>

using namespace std;

class Calculator {
public:
  int power(int, int);
};

int Calculator::power(int n, int p) {
  if (n < 0 || p < 0)
    throw domain_error("Values should be non-negative");
  else
    return pow(n, p);
}

int main() {
  Calculator myCalculator=Calculator();
  int n, p;
  cout << "Enter both positive base and power: ";
  if (scanf("%d %d",&n,&p) == 2) {
    try {
      int ans = myCalculator.power(n,p);
      cout << ans << endl;
    }
    catch (exception& e) {
      cout << e.what() << endl;
    }
  }
  return 0;
}

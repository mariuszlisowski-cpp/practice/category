// try me
// #try #throw @ catch

#include <iostream>

using namespace std;

void funct(int arg) {
   if (arg > 100) {
       throw -100.5;
   }
}

int main() {

   try {
      funct(101);
   } catch (double x) {
      cout << "Error: " << x << endl;
   }

  return 0;
}

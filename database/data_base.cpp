/*
Baza danych z użyciem struktur

DO POPRAWY!
rozmiar bazy zdefiniowany odgórnie tabelą (NIESKALOWALNY)
*/
#include <iostream>

using namespace std;

/* ---------------------------------------------------------------------- */
/* STRUKTURY                                                              */
/* ---------------------------------------------------------------------- */

struct Worker
{
  short age, payCheckNo, workerNo;
  string name, surname, sex;
};

struct Driver
{
  Worker worker;
  string cat;
  struct
  {
    short day, month, year;
  } licValidity;
};

struct Salesman
{
  Worker worker;
  short yearsOfExper, storeNo;
};

/* ---------------------------------------------------------------------- */
/* DEKLARACJE                                                             */
/* ---------------------------------------------------------------------- */

Driver exampleDriver();
Salesman exampleSalesman();
Driver createDriver();
Salesman createSalesman();
void viewDriver(Driver&, short, short);
void viewSalesman(Salesman&, short, short);
short chooseCathegory();
short chooseRecordsDriver();
short chooseRecordsSalesman();

/* ---------------------------------------------------------------------- */
/* PROGRAM                                                                */
/* ---------------------------------------------------------------------- */

int main()
{
  Driver drivers[10];     // DO POPRAWY!
  Salesman salesmen[10];  // DO POPRAWY!

  bool exit = false, err;
  short mode, cathegory;
  short recordsD = 0, recordsS = 0; // ilość kierowców i sprzedawców
  do {
    cout << endl;
    cout << "Dane osobowe w bazie danych..." << endl;
    cout << "1) dodwawanie rekordów" << endl;
    cout << "2) wyświetlanie rekordów" << endl;
    cout << "3) dodanie przykładowych rekordów" << endl;
    cout << "4) wyjście..." << endl;
    do
    {
      err = false;
      cout << ": ";
      cin >> mode;
      switch(mode)
      {
        case 1: /* dodawanie rekordów */
          cout << "Dodawanie rekordów..." << endl;
          cathegory = chooseCathegory();
          //records = chooseRecordsNo();
          if (cathegory == 1) // kierowcy
          {
            recordsD = chooseRecordsDriver();
            for (short i=0; i<recordsD; i++)
              drivers[i] = createDriver();
          }
          else // sprzedawcy
          {
            recordsS = chooseRecordsSalesman();
            for (short i=0; i<recordsS; i++)
              salesmen[i] = createSalesman();
          }
          break;
        case 2: /* wyświetlanie rekordów */
          cout << " Wyświetlanie rekordów..." << endl;
          cathegory = chooseCathegory();
          if (cathegory == 1) // kierowcy
            if (recordsD == 0)
              cout << "Baza pusta!" << endl;
            else
              for (short i=1; i<=recordsD; i++)
                viewDriver(drivers[i-1], i, recordsD);
          else // sprzedawcy
            if (recordsS == 0)
              cout << "Baza pusta!" << endl;
            else
              for (short i=1; i<=recordsS; i++)
                viewSalesman(salesmen[i-1], i, recordsS);
        break;
        case 3:
          drivers[0] = exampleDriver();
          salesmen[0] = exampleSalesman();
          recordsD = 1;
          recordsS = 1;
          cout << "Zrobione..." << endl;
        break;
        case 4:
          exit = true;
        break;
        default:
          cout << "Błędny wybór!" << endl;
          err = true;
          break;
      }
  } while (err);

  } while (!exit);

  return 0;
}

/* ---------------------------------------------------------------------- */
/* FUNKCJE                                                                */
/* ---------------------------------------------------------------------- */

/* przykładowy rekord kierowcy */
Driver exampleDriver()
{
  Driver driverEx;
  driverEx.worker.workerNo = 9;
  driverEx.worker.name = "Jan";
  driverEx.worker.surname = "Kowalski";
  driverEx.worker.sex = "mężczyzna";
  driverEx.worker.age = 49;
  driverEx.worker.payCheckNo = 33;
  driverEx.cat = "CE";
  driverEx.licValidity.day = 30;
  driverEx.licValidity.month = 6;
  driverEx.licValidity.year = 2030;
  return driverEx;
}

/* przykładowy rekord sprzedawcy */
Salesman exampleSalesman()
{
  Salesman salesmanEx;
  salesmanEx.worker.workerNo = 4;
  salesmanEx.worker.name = "Anna";
  salesmanEx.worker.surname = "Kowalska";
  salesmanEx.worker.sex = "kobieta";
  salesmanEx.worker.age = 27;
  salesmanEx.worker.payCheckNo = 66;
  salesmanEx.yearsOfExper = 8;
  salesmanEx.storeNo = 15;
  return salesmanEx;
}

/* wprowadzanie informacji o kierowcy */
Driver createDriver()
{
  Driver drv;
  cout << "Wprowadzanie danych kierowcy..." << endl;

  cout << "Numer pracownika?" << endl;
  cout << ": ";
  cin >> drv.worker.workerNo;

  cout << "Imię?" << endl;
  cout << ": ";
  cin >> drv.worker.name;

  cout << "Nazwisko?" << endl;
  cout << ": ";
  cin >> drv.worker.surname;

  cout << "Płeć?" << endl;
  cout << "1) mężczyzna" << endl;
  cout << "2) kobieta" << endl;
  short ans;
  bool err;
  do
  {
    err = false;
    cout << ": ";
    cin >> ans;
    switch (ans)
    {
      case 1: drv.worker.sex = "mężczyzna";
      break;
      case 2: drv.worker.sex = "kobieta";
      break;
      default:
        cout << "Błędny wybór!" << endl;
        err = true;
    }
  } while (err);

  cout << "Wiek?" << endl;
  cout << ": ";
  cin >> drv.worker.age;

  cout << "Numer na liście płac?" << endl;
  cout << ": ";
  cin >> drv.worker.payCheckNo;

  cout << "Kategoria prawa jazdy?" << endl;
  cout << "1) C" << endl;
  cout << "2) D" << endl;
  cout << "3) CE" << endl;
  cout << "4) DE" << endl;
  do
  {
    err = false;
    cout << ": ";
    cin >> ans;
    switch (ans)
    {
      case 1: drv.cat = "C";
      break;
      case 2: drv.cat = "D";
      break;
      case 3: drv.cat = "CE";
      break;
      case 4: drv.cat = "DE";
      break;
      default:
        cout << "Błędny wybór!" << endl;
        err = true;
    }
  } while (err);

  cout << "Ważność prawa jazdy?" << endl;
  /* dzień */
  do
  {
    err = false;
    cout << "~ dzień: ";
    cin >> ans;
    if ((ans < 1) || (ans > 31))
    {
      cout << "Miesiąc ma maksymalnie 31 dni!" << endl;
      err = true;
    }
    else drv.licValidity.day = ans;
  } while (err);
  /* miesiąc */
  do
  {
    err = false;
    cout << "~ miesiąc: ";
    cin >> ans;
    if ((ans < 1) || (ans > 12))
    {
      cout << "Rok ma 12 miesięcy!" << endl;
      err = true;
    }
    else drv.licValidity.month = ans;
  } while (err);
  /* miesiąc */
  do
  {
    err = false;
    cout << "~ rok: ";
    cin >> ans;
    if ((ans < 2019) || (ans > 2034 ))
    {
      cout << "Maksymalny okres ważności to 15 lat!" << endl;
      err = true;
    }
    else drv.licValidity.year = ans;
  } while (err);

  return drv;
}

/* wprowadzanie informacji o sprzedawcy */
Salesman createSalesman()
{
  Salesman sal;
  cout << "Wprowadzanie danych sprzedawcy..." << endl;

  cout << "Numer pracownika?" << endl;
  cout << ": ";
  cin >> sal.worker.workerNo;

  cout << "Imię?" << endl;
  cout << ": ";
  cin >> sal.worker.name;

  cout << "Nazwisko?" << endl;
  cout << ": ";
  cin >> sal.worker.surname;

  cout << "Płeć?" << endl;
  cout << "1) mężczyzna" << endl;
  cout << "2) kobieta" << endl;
  short ans;
  bool err;
  do
  {
    err = false;
    cout << ": ";
    cin >> ans;
    switch (ans)
    {
      case 1: sal.worker.sex = "mężczyzna";
      break;
      case 2: sal.worker.sex = "kobieta";
      break;
      default:
        cout << "Błędny wybór!" << endl;
        err = true;
    }
  } while (err);

  cout << "Wiek?" << endl;
  cout << ": ";
  cin >> sal.worker.age;

  cout << "Numer na liście płac?" << endl;
  cout << ": ";
  cin >> sal.worker.payCheckNo;

  cout << "Lata doświadczenia?" << endl;
  cout << ": ";
  cin >> sal.yearsOfExper;

  cout << "Nr sklepu?" << endl;
  cout << ": ";
  cin >> sal.storeNo;

  return sal;
}

/* wyświetlanie informacji o kierowcy */
void viewDriver(Driver &drv, short n, short k)
{
  cout << endl;
  cout << "Informacje o pracownikach... " << endl;
  cout << "[ rekord " << n << " / " << k << " ]" << endl;
  cout << "Kategoria:\t\tkierowcy" << endl;
  cout << "Nr kierowcy:\t\t" << drv.worker.workerNo << endl;
  cout << "Imię:\t\t\t" << drv.worker.name << endl;
  cout << "Nazwisko:\t\t" << drv.worker.surname << endl;
  cout << "Płeć:\t\t\t" << drv.worker.sex << endl;
  cout << "Wiek:\t\t\t" << drv.worker.age << endl;
  cout << "Nr na liście płac:\t" << drv.worker.payCheckNo << endl;
  cout << "Kat. prawa jazdy: \t" << drv.cat << endl;
  cout << "Data ważności:\t\t" << drv.licValidity.day << "/"
                               << drv.licValidity.month << "/"
                               << drv.licValidity.year << endl;
}

/* wyświetlanie informacji o sprzedawcy */
void viewSalesman(Salesman &sal, short n, short k)
{
  cout << endl;
  cout << "Informacje o pracownikach... " << endl;
  cout << "[ rekord " << n << " / " << k << " ]" << endl;
  cout << "Kategoria:\t\tsprzedawcy" << endl;
  cout << "Nr kierowcy:\t\t" << sal.worker.workerNo << endl;
  cout << "Imię:\t\t\t" << sal.worker.name << endl;
  cout << "Nazwisko:\t\t" << sal.worker.surname << endl;
  cout << "Płeć:\t\t\t" << sal.worker.sex << endl;
  cout << "Wiek:\t\t\t" << sal.worker.age << endl;
  cout << "Nr na liście płac:\t" << sal.worker.payCheckNo << endl;
  cout << "Lata doświadcznia: \t" << sal.yearsOfExper << endl;
  cout << "Nr sklepu zatrudnienia:\t" << sal.storeNo << endl;
}

/* wybór kategorii pracowników */
short chooseCathegory()
{
  cout << "Kategoria pracowników?" << endl;
  cout << "1) kierowcy" << endl;
  cout << "2) sprzedawcy" << endl;
  short cat;
  bool err;
  do
  {
    err = false;
    cout << ": ";
    cin >> cat;
    if ((cat != 1) && (cat != 2 ))
    {
      cout << "Niepoprawny wybór!" << endl;
      err = true;
    }
  } while (err);
  return cat;
}

/* ilość rekordów kierowców */
short chooseRecordsDriver()
{
  short recs;
  cout << "Ilość kierowców do wprowadzenia: ";
  cin >> recs;
  return recs;
}

/* ilość rekordów sprzedawców */
short chooseRecordsSalesman()
{
  short recs;
  cout << "Ilość sprzedawców do wprowadzenia: ";
  cin >> recs;
  return recs;
}

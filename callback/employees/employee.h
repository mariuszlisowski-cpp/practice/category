#pragma once

#include <iostream>
#include <string>

class Employee {
public:
    Employee(const std::string& name, unsigned age);

    std::string& name() &;
    const std::string& name() const &;

    unsigned& age() &;
    const unsigned& age() const &;

    // friend std::ostream& operator<<(std::ostream& os, const Employee& obj);
    
private:
    std::string name_;
    unsigned age_;
};

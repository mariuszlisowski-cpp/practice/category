#include <iostream>

void funcA() {
    std::cout << "Hello!" << std::endl;
}

void funcB(void(*ptr)()) {
    ptr();                                                      // call-back of function passed as argument
}

int main() {
    void(*f)() = funcA;                                         // signature of function A
    funcB(f);

    return 0;
}

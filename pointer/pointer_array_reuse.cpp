#include <iostream>

int main() {
    int* ptr;
    
    // pointer dynamic initialization
    ptr = new int[2];

    *ptr = 99;
    *(ptr + 1) = 88;

    for (size_t i = 0; i < 2; ++i) {
        std::cout << *(ptr + i) << std::endl;
    }

    delete [] ptr;

    // pointer re-used (another init)
    ptr = new int[2];
    *ptr = 55;
    *(ptr + 1) = 77;

    for (size_t i = 0; i < 2; ++i) {
        std::cout << *(ptr + i) << std::endl;
    }

    delete [] ptr;

    return 0;
}

// function pointer array

#include <iostream>

using namespace std;

int addF(int, int);
int mulF(int, int);
int divF(int, int);

int main() {
   const int max = 3;
   int A = 6;
   int B = 3;


   // function pointer array prototype
   int (*pFunctArray[max])(int, int);

   cout << "Add | Multiple | Divide" << endl;
   pFunctArray[0] = addF; // adding
   pFunctArray[1] = mulF; // multipyling
   pFunctArray[2] = divF; // dividing

   for (int i = 0; i < max; i++) {
      switch (i) {
         case 0:
            cout << A << " + " << B << " = " << pFunctArray[i](A, B) << endl;
            break;
         case 1:
            cout << A << " * " << B << " = " << pFunctArray[i](A, B) << endl;
            break;
         case 2:
            cout << A << " / " << B << " = " << pFunctArray[i](A, B) << endl;
            break;
      }
   }

  return 0;
}

int addF(int x, int y) {
   return x + y;
}

int mulF(int x, int y) {
   return x * y;
}

int divF(int x, int y) {
   return x / y;
}

#include <iostream>
#include <memory>

int main() {
    // default initialization
    int x_garbage;
    int* ptr_garbage = new int;

    std::cout << "> ptr_garbage: " << x_garbage << std::endl;
    std::cout << "> ptr_garbage: " << *ptr_garbage << std::endl;
    delete ptr_garbage;

    // value initializatoin
    int x_zeroed{}; // c++11
    int x_valued{9}; // c++11
    int* ptr_zeroed = new int();
    int* ptr_valued_modern = new int{}; // c++11
    std::shared_ptr<int> shared_zeroed = std::make_shared<int>();
    std::shared_ptr<int> shared_valued = std::make_shared<int>(9);

    std::cout << "> zeroed: " << x_zeroed << std::endl;
    std::cout << "> valued: " << x_valued << std::endl;
    std::cout << "> zeroed: " << *ptr_zeroed << std::endl;
    std::cout << "> zeroed: " << *shared_zeroed << std::endl;
    std::cout << "> valued: " << *shared_valued << std::endl;


    return 0;
}

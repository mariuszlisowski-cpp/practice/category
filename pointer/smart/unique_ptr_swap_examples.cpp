/* unique_ptr swap examples
   Exchanges the contents of the unique_ptr object with those of x,
   transferring ownership of any managed object between them without destroying either
 */
#include <iostream>
#include <memory>

int main() {
    std::unique_ptr<int> foo(new int(42));
    std::unique_ptr<int> bar(new int(24));

    foo.swap(bar);

    std::cout << "foo: " << *foo << ", ";
    std::cout << "bar: " << *bar << '\n';

    std::swap(foo, bar);

    std::cout << "foo: " << *foo << ", ";
    std::cout << "bar: " << *bar << '\n';

    return 0;
}

// function pointer

#include <iostream>

using namespace std;

int addF(int, int);
int mulF(int, int);
int divF(int, int);

int main() {
   int A = 6;
   int B = 3;

   // function pointer prototype
   int (*pFunct)(int, int);

   cout << "1 - Add | 2 - Multiple | 3 - Divide" << endl;
   int choice;
   cin >> choice;
   switch (choice) {
      case 1: pFunct = addF; break; // adding
      case 2: pFunct = mulF; break; // multipyling
      case 3: pFunct = divF; break; // dividing
   }

   cout << A << " | + | * | / | " << B << " =  " << pFunct(A, B) << endl;

  return 0;
}

int addF(int x, int y) {
   return x + y;
}

int mulF(int x, int y) {
   return x * y;
}

int divF(int x, int y) {
   return x / y;
}

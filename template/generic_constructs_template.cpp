// generic constructs
// #template #typename #T #A

#include <iostream>
#include <vector>
#include <string>

using namespace std;

// function for printing different data types
template <typename T, typename A>
void printArray(vector<T, A> const&);

int main() {
  vector<int> intVec { 4, 5, 3, 6, 9 };
  vector<float> floatVec { 4.5, 7.4, 4.2, 5.7, 3.6 };
  vector<char> charVec { 'd', 't', 't', 'l', 'q' };
  vector<string> stringVec { "ah", "je", "de", "kd", "ke" };

  printArray<int> (intVec);
  printArray<float> (floatVec);
  printArray<char> (charVec);
  printArray<string> (stringVec);

    return 0;
}

template <typename T, typename A>
void printArray(vector<T, A> const& arr) {
  for (auto i : arr)
    cout << i << " ";
  cout << endl;
}

#include <iostream>

template <typename Type,
          template <typename TYPE> class TClass>
void func(TClass<Type>& tc) {
    if (tc.somethingTrue())
        tc.doStuff();
}

template <class T>
class Clazz {
public:
    bool somethingTrue() {
        return true;
    }
    void doStuff() {
        std::cout << "> stuff done!" << std::endl;
    }
private:
    T value{};
};

int main() {
    Clazz<int> obj;
    func(obj);

    return 0;
}

#include <iostream>
#include <map>

int main() {
    std::map<char, int> letters{
        { 'c', 3 },
        { 'a', 1 },
        { 'd', 4 },
        { 'b', 2 }
    };

    auto first_letter = letters.begin()->first;
    auto last_letter = letters.rbegin()->first;

    std::cout << first_letter << std::endl;
    std::cout << last_letter << std::endl;

    return 0;
}

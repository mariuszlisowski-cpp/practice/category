#include <algorithm>
#include <iostream>
#include <iterator>
#include <fstream>
#include <vector>
#include <string>

class line { 
public:
    operator std::string() {
        return data;
    }
    friend std::istream& operator>>(std::istream& is, line& d) { 
        return std::getline(is, d.data);
    }

private:
    std::string data;
};

int main() {
    std::fstream file("read_file_iterator.cpp", std::ios::in);

    std::vector<std::string> lines(std::istream_iterator<line>(file),
                                   std::istream_iterator<line>());

    
    // for (const auto& line : lines) {                                     // no iterators (!)
    //     std::cout << line << std::endl;
    // }

    // std::copy(begin(lines), end(lines),
    //           std::ostream_iterator<std::string>(std::cout, "\n"));

    return 0;
}

#include <array>
#include <iostream>

template<typename T, typename... Args>
auto create_array(Args&&... args) -> std::array<T, sizeof... (args)> {
    return { std::forward<Args>(args)... };
}

int main() {
    auto arr = create_array<int>(7, 6, 5, 4, 3, 2, 1);

    for (size_t i{}; i < arr.size(); std::cout << arr[i++]);

    return 0;
}

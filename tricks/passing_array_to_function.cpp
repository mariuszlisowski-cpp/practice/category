#include <iostream>

/* accepting array type (with its size!) */
template<typename T>
void funct(T& param) {                          // param type is: const int(&)[3]
    for (auto el : param) {                     // known size!
        std::cout << el << ' ';
    }
}

int main() {
    int array[] = { 1, 2, 3};                   // deducted size

    funct(array);                               // argument of an array

    return 0;
}

#include <algorithm>
#include <ios>
#include <iostream>
#include <string>

int main() {
    std::string s{"rota#@ator"};

    auto [from_left, from_right] = std::mismatch(s.cbegin(), s.cend(),
                                                 s.crbegin());

    if (from_left != s.end()) {
        std::cout << "from left, mismatch at: " << *from_left << std::endl; 
    } else {
        std::cout << "palindrome" << std::endl;
    }

    if (from_right != s.rend()) {
        std::cout << "from right, mismatch at: " << *from_right << std::endl;
    } else {
        std::cout << "palindrome" << std::endl;
    }

    return 0;
}

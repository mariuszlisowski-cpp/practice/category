#include <array>
#include <iostream>
#include <map>
#include <string>

struct Foo {
    Foo(int i) {}
};

int main() {
    std::map<std::string, int> mp;

    std::cout << mp.size() << std::endl;
    mp["Inserts new element!"];
    std::cout << mp.size() << std::endl;

    std::map<std::string, Foo> custom;
    custom["Cannot insert as no default c'tor"];

    return 0;
}

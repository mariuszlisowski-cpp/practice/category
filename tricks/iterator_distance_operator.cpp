#include <algorithm>
#include <iostream>
#include <string>
 
int main() {
    std::string str = "Lorem ipsum";
    const char LETTER = 'i';

    auto it = std::find(str.begin(), str.end(), LETTER);
    std::cout << "length: "
              << str.end() - str.begin() << '\n';

    /* calculate index */
    std::cout << LETTER << " found at offset: "
              << it - str.begin() << '\n';                          // operator-

    std::cout << LETTER << " found at offset: "
              << std::distance(str.begin(), it) << '\n';            // helper function

    return 0;
}

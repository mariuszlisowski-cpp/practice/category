#include <iostream>

void foo(const int& value)
{
    // value = 42;                                                          // error as value is const
    const_cast<int&>(value) = 42;
    int result = value * 2;
 
    std::cout << result << std::endl;
}

void bar(int& value) {}

int main()
{
    const int a{ 5 };
    foo(a);

    /* cannot be called as argument is const but functions parameter is not
    bar(a); */
}

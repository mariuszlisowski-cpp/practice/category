#include <iostream>
#include <type_traits>
#include <string>

class Empty {};                     // trivial
class Test {                        // non-trivial
    Test() {}
};

int main() {
    std::cout << std::boolalpha;
   
    /* trivial data types */
    std::cout << std::is_trivial<int>::value << std::endl;
    std::cout << std::boolalpha << std::is_trivial<Empty>::value << std::endl;
    
    /* non-trivial data types */
    std::cout << std::boolalpha << std::is_trivial<std::string>::value << std::endl;
    std::cout << std::boolalpha << std::is_trivial<Test>::value << std::endl;

    return 0;
}

#include <iostream>

int main() {
    int arr[]{ 99, 88, 77 };
    auto size = sizeof(arr) / sizeof(*arr);
    
    for (size_t i = 0; i < 21; ++i) {
        std::cout << arr[i % size] << ' ';
    }

    return 0;
}

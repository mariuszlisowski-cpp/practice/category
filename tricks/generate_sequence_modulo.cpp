#include <iostream>
#include <limits>
#include <sys/types.h>

void generate_sequence(uint seq_max, uint elements) {
    for (size_t i = 0; i < 20; ++i) {
        std::cout << i % (seq_max + 1) << ' ';              // 0 - seq_max
    }
    std::cout << '\n';
}

int main() {
    generate_sequence(5, 20);                               // generate n of elements

    return 0;
}

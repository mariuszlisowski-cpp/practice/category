/*
Digit find
#this #klasa pochodna #przeciążenie operatora
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

/* klasa dziedziczy z klasy bazowej 'string' */
class MyString : public string {
public:
  MyString() {}
  ~MyString() {}

  // metoda klasy
  void showDigit();

  // przeciążenie operatora przypisania (literał typu char)
  MyString operator = (const char* text) {
    this->assign(text); // przypisuje 'text' do bieżącego obiektu
    return *this; // wyłuskuje bieżący obiekt i go zwraca
  }
  // przeciążenie operatora przypisania (literał typu string)
  MyString operator = (string text) {
    this->assign(text); // nazwa typu string jest już wskaźnikiem
    return *this;
  }
};

/* implementacja metody klasy */
void MyString::showDigit() {
  string digits("1234567890");

  // this odnosi się do bieżącego obiektu klasy
  for (int i = 0; i < this->size(); i++) {
    if (digits.find(this->at(i)) != string::npos) {
      cout << "W ciągu \"" << *this << "\" znalazłem cyfrę "
           << this->at(i) << " na pozycji " << i << endl;
    }
  }
}

int main()
{
  MyString str;
  str.append("raisin.77"); // metoda z klasy bazowej 'string'
  str.showDigit(); // nowa metoda z klasy pochodnej

  char c[8] = "Mariusz";
  cout << (str = c) << endl; // przypisuje literał (tablica char)
  cout << (str = ">raisin<") << endl; // j.w.

  string s("Lisowski");
  cout << (str = s) << endl; // przypisuje literał (string)

  return 0;
}

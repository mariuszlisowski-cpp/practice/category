// operator overloading (increment)
// #prefix #postfix #operator++()

#include <iostream>

using std::cout;
using std::endl;
using std::string;

class Counter {
private:
  int count;
public:
  // default constructor (inline)
  Counter():count(0) {};
  // destructor (inline)
  ~Counter() {};

  // accessors
  // setter (inline)
  void setCount(int c) {
    count = c;
  }
  // getter (inline)
  int getCount() {
    return count;
  }

  // operator overloading increment (prefix)
  const Counter& operator++(); // without parameters

  // operator overloading increment (postfix)
  const Counter operator++(int); // parameter int NOT used
};

// retruns a reference (no need to use any temporary value)
// prefix: increment a value and return it
const Counter& Counter::operator++() {
  ++count;
  return *this; // returns an object (defererence)
}

// returns a value (temporary value must be used)
// postfix: return a value then increment it
const Counter Counter::operator++(int) {
  Counter temp(*this); // temporary object with a current value (defererenced)
  ++count; // value is incremented
  return temp; // returns pre-incremented value
}

void divider();

using namespace std;

int main() {
  // prefix
  Counter preIncrement; // default constructor (setting count to 0)
  cout << preIncrement.getCount() << endl;
  ++preIncrement;
  ++preIncrement;
  cout << preIncrement.getCount() << endl;
  divider();
  // assigning an overloaded operator method
  Counter object = ++preIncrement; // overloading retruns an object;
  cout << preIncrement.getCount() << endl;
  divider();

  // postfix
  Counter postIncrement;
  cout << postIncrement.getCount() << endl;
  postIncrement++;
  postIncrement++;
  cout << postIncrement.getCount() << endl;
  divider();

  // comparision (prefix vs postfix)
  preIncrement.setCount(0);
  postIncrement.setCount(0);
  ++preIncrement;
  postIncrement++;
  cout << preIncrement.getCount() << endl;
  cout << postIncrement.getCount() << endl;
  divider();
  // but...
  Counter pre;
  Counter post;
  pre = ++preIncrement; // returns incremented value
  post = postIncrement++; // first retruns a current value then increments
  cout << pre.getCount() << endl; // gets 2
  cout << preIncrement.getCount() << endl; // also gets 2
  cout << post.getCount() << endl; // gets 1 (current value)
  cout << postIncrement.getCount() << endl; // gets 2 (already incremented)

  return 0;
}

void divider() {
  cout << string(5, '-') << endl;
}

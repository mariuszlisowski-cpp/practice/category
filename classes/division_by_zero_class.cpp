// division by zero class

#include <iostream>                                                             
#include <string>

class DivisionByZeroException : public std::exception {
public:
    // DivisionByZeroException() = delete;
    explicit DivisionByZeroException() {
        buildErrorMessage();
    }
    virtual const char* what() const noexcept override {
        return errorMessage.c_str();
    }
private:
    std::string errorMessage;

    void buildErrorMessage() {
        errorMessage = "Division by zero prohibited!";
    }
};

double divide(const double dividend, const double divisor) {
    if (divisor == 0) {
        throw DivisionByZeroException();
    }
    return dividend / divisor;
}

int main() {
    double dividend = 10;
    double divisor = 0;
    try {
        std::cout << divide(dividend, divisor) << std::endl;
    } catch (const DivisionByZeroException& ex) {
        std::cerr << ex.what() << std::endl;
        return 1;
    }

    return 0;
}

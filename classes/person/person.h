// class header

#pragma once

using std::string;

class Person {
private:
  // variables
  string firstName;
  string lastName;
  int age;
public:
  // constructors
  Person();
  Person(string, string);
  Person(string, string, int);

  // destructor
  ~Person();

  // accessors
  string getFirstName();
  string getLastName();
  int getAge();
  void setFirstName(string fName);
  void setLastName(string lName);
  void setAge(int age);

  // methods
  string sayHello();
};

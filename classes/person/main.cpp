// Person

#include <iostream>
#include "person.h"

using std::cout;
using std::endl;
using std::string;

int main() {

  Person* pOne = new Person();
  Person* pTwo = new Person("Mariusz", "Lisowski");
  Person* pThr = new Person("Mateusz", "Lisowski", 12);

  cout << "Person " << pTwo->getFirstName() << " " << pTwo->getLastName()
       << " says " << pTwo->sayHello() << "!" << endl;

  cout << "Person " << pThr->getFirstName() << " " << pTwo->getLastName()
       << " says " << pThr->sayHello() << "!" << endl;
  cout << pThr->getFirstName() << " is aged " << pThr->getAge() << endl;

  pOne->setFirstName("Milosz");
  pOne->setLastName("Lisowski");
  pOne->setAge(9);

  cout << "Person " << pOne->getFirstName() << " " << pOne->getLastName()
       << " says " << pOne->sayHello() << "!" << endl;
  cout << pOne->getFirstName() << " is aged " << pOne->getAge() << endl;

  delete pOne;
  delete pTwo;
  delete pThr;

  return 0;
}

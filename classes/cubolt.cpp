/*
Cubold
~ pole powierzchni prostopadłościanu
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

class Cubold {
private:
  float a, b, c;
public:
  Cubold();
  Cubold(float, float, float);

  ~Cubold() {}

  float getA() { return a; }
  float getB() { return b; }
  float getC() { return c; }

  float surfaceArea();
};

Cubold::Cubold():a(1), b(1), c(1) {}
Cubold::Cubold(float x, float y, float z):a(x), b(y), c(z) {}

float Cubold::surfaceArea() {
  return 2*(a*b + b*c + a*c);
}

int main()
{
  Cubold cuboldA;
  cout << "Pole powierzchni prostopadłościoanu o wymiarach "
       << cuboldA.getA() << "x" << cuboldA.getB() << "x" << cuboldA.getC()
       << " wynosi " << cuboldA.surfaceArea() << endl;

  Cubold cuboldB(2.5, 3.2, 4.8);
  cout << "Pole powierzchni prostopadłościoanu o wymiarach "
       << cuboldB.getA() << "x" << cuboldB.getB() << "x" << cuboldB.getC()
       << " wynosi " << cuboldB.surfaceArea() << endl;

  return 0;
}

// operator overloading (equals)
// #operator=()

#include <iostream>

class Bike {
private:
  int* speed; // pointer to int
public:
  // default constructor
  Bike();
  // overloaded constructor
  Bike(int);
  // destructor (inline)
  ~Bike() {}

  // accessor (must be constant)
  int getSpeed() const {
    return *speed;
  }

  // overloaded operator (=)
  Bike operator=(const Bike&);
};

Bike::Bike() {
  speed = new int;
  *speed = 0;
}

Bike::Bike(int n) {
  speed = new int;
  *speed = n;
}

// operator overloading implementation
Bike Bike::operator=(const Bike& rhs) {
  if (this == & rhs)
    return *this; // if lhs & rhs operands are the same
  delete speed; // good practice to avoid memory leaks
  speed = new int; // new memory address created
  *speed = rhs.getSpeed();
  return *this; // returns the value at speed memory location (newly created)
}

using namespace std;

int main() {
  Bike yamaha(290);
  cout << yamaha.getSpeed() << endl;

  Bike suzuki(180);
  cout << suzuki.getSpeed() << endl;

  Bike honda;
  cout << honda.getSpeed() << endl;

  // operator overloading (=)
  honda = yamaha;
  cout << honda.getSpeed() << endl;

  honda = honda; // lhs & rhs are the same (no need to delete a pointer)
  cout << honda.getSpeed() << endl;

  return 0;
}

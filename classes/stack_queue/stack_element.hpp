// StackElement class header

#pragma once

class StackElement {
public:
  char data;
  StackElement* under = nullptr;
  StackElement(char ch) {
    data = ch;
    under = nullptr;
  }
};

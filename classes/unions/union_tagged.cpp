#include <cassert>

enum ShapeKind { Square, Rectangle, Circle };

struct Shape {
    int centerx;
    int centery;
    enum ShapeKind kind;
    union {
        struct { int side; }           squareData;
        struct { int length, height; } rectangleData;
        struct { int radius; }         circleData;
    } shapeKindData;
};

int getSquareSide(struct Shape* s) {
    assert(s->kind == Square);
    return s->shapeKindData.squareData.side;
}

void setSquareSide(struct Shape* s, int side) {
    s->kind = Square;
    s->shapeKindData.squareData.side = side;
}

int main() {
    Shape* shape = new Shape();

    setSquareSide(shape, 42);
    getSquareSide(shape);

    return 0;
}

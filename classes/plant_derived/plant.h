// base class Plant header

using std::string;

class Plant {
private:
  string species;
protected:
  int age;
public:
  // constructors
  Plant();
  Plant(string, int);
  // destructor
  ~Plant();
  // accessors
  void setSpecies(string);
  string getSpecies();
  // methods

};

// Base Plant class and derived Tree class

#include <iostream>
#include "plant.h"
#include "tree.h"

using std::cout;
using std::endl;

int main() {
  cout << "------------------------------------------" << endl;
  Plant largePlant;
  cout << largePlant.getSpecies() << endl;
  largePlant.setSpecies("flower");
  cout << largePlant.getSpecies() << endl << endl;


  Tree largeTree;
  cout << largeTree.getName() << " " << largeTree.getAge() << endl;
  largeTree.getOlder(5); // accessing protected variable
  cout << largeTree.getName() << " " << largeTree.getAge() << endl << endl;

  Tree smallTree("pine", 5); // name & height parameters
  cout << smallTree.getName() << " " << smallTree.getHeight() << endl;
  smallTree.getHigher(5);
  cout << smallTree.getName() << " " << smallTree.getHeight() << endl << endl;

  Tree mediumTree("tree", 8, "birch", 13); // species, age, name & height
  cout << mediumTree.getName() << " " << mediumTree.getAge() << endl;
  mediumTree.getOlder(4);
  cout << mediumTree.getName() << " " << mediumTree.getAge() << endl << endl;

  cout << "------------------------------------------" << endl;
  return 0;
}

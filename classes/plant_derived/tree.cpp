// derived class Tree implementation

#include <iostream>
#include "plant.h"
#include "tree.h"


using std::string;
using std::cout;
using std::endl;

// no argument constructor
Tree::Tree():name("oak"), height(20) {
  cout << "class Tree DEFAULT constructor..." << endl;
}
// parameterized constructor for Tree class only
Tree::Tree(string name, int height):name(name), height(height) {
  cout << "class Tree ARGUMENT constructor..." << endl;
}
// parameterized constructor for Tree and base Plant class
Tree::Tree(string species, int age, string name, int height)
  :Plant(species, age), name(name), height(height) {
  cout << "class Tree & Plant ARGUMENT constructor..." << endl;
}
// destructor
Tree::~Tree () {
  cout << "class Tree destructor..." << endl;
}
// setter
void Tree::setName(string name) {
  this->name = name;
}
// getter
string Tree::getName() {
  return name;
}
// setter
void Tree::setHeight(int height) {
  this->height = height;
}
// getter
int Tree::getHeight() {
    return height;
}
// method
int Tree::getHigher(int higher) {
  return height += higher;
}
// method
int Tree::getOlder(int older) {
  return age += older; // protected variable accessible for a derived class
}
// method
int Tree::getAge() {
  return age;
}

#include <iostream>

class Base {
public:
    virtual ~Base() {}
    virtual void print() const {
        std::cout << "base" << std::endl;
    }
};

class Derived : public Base {
public:
    void print() const override {
        std::cout << "derived" << std::endl;

    }
};

int main() {
    Base* base_ptr{};

    Base base;
    Derived derived;
    base.print();

    base_ptr = &derived;
    base_ptr->print();

    return 0;
}

// operator overloading (plus)
// #operator+()

#include <iostream>

using namespace std;

class Value {
  private:
    float val;

  public:
    // default constructor (inline)
    Value() : val(0) {}
    // overloaded constructor (inline)
    Value(float f) : val(f) {}
    // destructor (inline)
    ~Value() {}

    // accessor
    float getVal() const;

    // overloaded operator (+)
    Value operator+(const Value&);
};

// getter
float Value::getVal() const { return val; }

// operator overloading implementation
Value Value::operator+(const Value& rhs) { return Value(val + rhs.getVal()); }

int main() {
    Value scoreA(10);
    Value scoreB(15);
    Value total;

    // operator overloading (+)
    total = scoreA + scoreB;
    cout << total.getVal() << endl;
    // alternatively
    total = scoreA.operator+(scoreB);
    cout << total.getVal() << endl;

    return 0;
}

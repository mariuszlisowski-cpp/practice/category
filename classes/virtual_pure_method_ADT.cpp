// abstract data type (ADT) & pure virtual methods
// #ADT #polymorphism #pure_virtual

#include <iostream>

using namespace std;

// abstract class
class Shape {
public:
   Shape() {}
   virtual ~Shape() {
      cout << "~ Shape dectructor" << endl;
   }

   // pure virtual methods
   virtual int getArea() = 0; // causes the class abstract
   virtual void draw() = 0; // can be and IS implemented below
   void info () {
      cout << ": Shape class" << endl;
   }
};

void Shape::draw() {
   cout << ": drawing additinal shape..." << endl;
}

class Rectangle : public Shape {
public:
   Rectangle():width(1), length(1) {}
   Rectangle(int w, int l):width(w), length(l) {}
   ~Rectangle() {
      cout << "~ Rectangle dectructor" << endl;
   }

   // pure virtual methods
   virtual int getArea() {
      return width * length;
   }
   virtual void draw();
private:
   int width;
   int length;
};

// pure virtual method implemented
void Rectangle::draw() {
   cout << "Drawing a rectangle..." << endl;
   Shape::draw(); // pure virtual base method call
}

class Square : public Rectangle {
public:
   Square():width(1) {}
   Square(int w):width(w) {}
   ~Square() {
      cout << "~ Square dectructor" << endl;
   }

   // pure virtual methods
   virtual int getArea() {
      return width * width;
   }
   void draw(); // does not have to be virtual (unless inherited)
private:
   int width;
};

// pure virtual method implemented
void Square::draw() {
   cout << "Drawing a square..." << endl;
   Shape::draw(); // pure virtual base method call
}

void divider();

int main() {
   Shape* pSh;
   divider();

   // abstract class object CANNOT be created
   // pSh = new Shape; // compiler error

   pSh = new Rectangle(2, 3);
   pSh->draw(); // derived class method call
   cout << "Rectangle area: " << pSh->getArea() << endl;
   pSh->info(); // base class method call
   divider();

   pSh = new Square(5);
   pSh->draw();
   cout << "Square area: " << pSh->getArea() << endl;
   pSh->info();
   divider();

   delete pSh;

   return 0;
}

void divider() {
   cout << string(28, '-') << endl;
}

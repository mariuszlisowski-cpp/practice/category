// class Book header with friend functions

class Book {
private:
  float price;
protected:
  int pages;
public:
  // constructors
  Book();
  Book(float, int);
  // destructor
  ~Book();
  // accessors
  void setPrice(float);
  float getPrice();
  int getPages();

  // friend function
  friend void halfPrice(Book&);
};

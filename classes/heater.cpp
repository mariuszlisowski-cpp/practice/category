/*
Heater
#kontruktor kopiujący
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

class Heater {
private:
  int *temperature;
public:
  /* deklaracja konstruktorów */
  Heater(); // domyślny
  Heater(const Heater&); // kopiujący

  /* deklaracja destruktora */
  ~Heater();

  /* akcesory (inline) */
  int getTemperature() const { return *temperature; } //  (const !)
  void setTemperature(int newTemperature) { *temperature = newTemperature; }

  /* deklaracja metod */
  void increaseTemperature(int degrees);
  void decreaseTemperature(int degrees);
};

/* implementacja konstruktora domyślnego */
Heater::Heater() {
  temperature = new int; // przydzielenie adresu na zmienną typu int
  *temperature = 21; // domyślna temperatura
}

/* implementacja konstruktora kopiującego */
Heater::Heater(const Heater& rhs) { // rigth-hand-side (const !)
  temperature = new int; // kolejny adres na zmienną
  *temperature = rhs.getTemperature();
}

/* implementacja destruktora */
Heater::~Heater () {
   delete temperature;
   temperature = NULL; // dla bezpieczeństwa
}

/* implementacja metody */
void Heater::increaseTemperature(int degrees) {
  setTemperature(*temperature + degrees);
  cout << "Cieplej o " << degrees << "..." << endl;
}

/* implementacja metody */
void Heater::decreaseTemperature(int degrees) {
  setTemperature(*temperature - degrees);
  cout << "Chłodniej o " << degrees << "..." << endl;
}

/* deklaracja funkcji */
void line();
void showTemperature(Heater);

/* ---------------------- FUNCKA MAIN ---------------------- */
int main() {

  /* 1 */
  line(); // konstruktor domyślny
  Heater floorHeater;
  showTemperature(floorHeater); // temperatura domyślna
  floorHeater.increaseTemperature(4);
  showTemperature(floorHeater);

  /* 2 */
  line();  // konstruktor kopiujący
  Heater wallHeater(floorHeater); // temperatura skopiowana
  showTemperature(wallHeater);

  /* 3 */
  line();  // dwa wskaźniki przechowujące temperaturę
  floorHeater.decreaseTemperature(6);
  showTemperature(floorHeater);
  wallHeater.increaseTemperature(3);
  showTemperature(wallHeater);

  /* 4 */
  line();  // użycie akcesorów
  floorHeater.setTemperature(30);
  showTemperature(floorHeater);
  wallHeater.setTemperature(19);
  showTemperature(wallHeater);


  return 0;
}
/* --------------------------------------------------------- */

/* implementacja funkcji */
void line() {
  static int counter = 0;
  counter ++;
  cout << "----------------------------------------------" << endl;
  cout << counter << "." << endl;
}

void showTemperature(Heater heater) {
  cout << "Temperatura wynosi: " << heater.getTemperature() << endl;
}

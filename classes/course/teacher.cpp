// class Teacher implementation

#include <iostream>
#include "teacher.h"

using std::cout;
using std::endl;
using std::string;

Teacher::Teacher() {}
Teacher::Teacher(string fName, string lName, int age, string address, string city, int phone):firstName(fName), lastName(lName), age(age), address(address), city(city), phone(phone) {}
Teacher::~Teacher() {}

void Teacher::setFirstName(string fName) { firstName = fName; }
void Teacher::setLastName(string lName) { lastName = lName; }
void Teacher::setAge(int age) { age = age; }
void Teacher::setAddress(string address) { address = address; }
void Teacher::setCity(string city) { city = city; }
void Teacher::setPhone(int phone) { phone = phone; }

string Teacher::getFirstName() { return firstName; }
string Teacher::getLastName() { return lastName; }
int Teacher::getAge() { return age; }
string Teacher::getAddress() { return address; }
string Teacher::getCity() { return city; }
int Teacher::getPhone() { return phone; }

void Teacher::GradeStudent() {
  cout << "Student graded" << endl;
}

void Teacher::SitInClass() {
  cout << "Sitting at front of class" << endl;
}

//  palindrome removal form vector
// #erase #remove

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

class IsPalindrome {
public:
    bool operator()(const std::string& word) {
        const auto middleOfWord = std::begin(word) + word.size() / 2;
        return std::equal(std::begin(word),
                          middleOfWord,
                          std::rbegin(word));
    }
};

int main() {
    std::vector<std::string> someWords{ "rotator",
                                        "dad",
                                        "radar",
                                        "foo",
                                        "hello" };

    someWords.erase(std::remove_if(std::begin(someWords),
                                   std::end(someWords),
                                   IsPalindrome()),
                    std::end(someWords));
    std::for_each(std::begin(someWords),
                  std::end(someWords),
                  [](const auto& word) {
                      std::cout << word << ", ";
                  });

    return 0;
}

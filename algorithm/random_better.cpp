/*
Dzieli dostępny zakres losowy na równe części i wpsi
n.p.
n = 3
RAND_MAX = 32767
bucket_size = 10922
rand [0     - 10922] => kodowane jako 0
rand [10922 - 21844] => kodowane jako 1
rand [21844 - 32766] => kodowane jako 2
rand [32766 - 32767] => ignorowane
*/

#include <iostream>
#include <ctime>
#include <iomanip> // std::setw
#include <unistd.h> // usleep

int nrand(int);

int main()
{
  int zakres;
  std::cout << "Liczba losowa z zakresu (0-n)..." << std::endl;
  std::cout << "Limit zakresu: " << RAND_MAX << std::endl << ": ";
  std::cin >> zakres;
  std::cout << "  ## STOP: CTRL-C ##" << std::endl;

  int losowych = 10;
  srand(time(NULL));

  while (true) {
    for (int i = 0; i < losowych; i++)
      std::cout << std::right << std:: setw(3) << nrand(zakres+1) << " ";
    std::cout << std::endl;
    usleep(500000); // 0.5s
}
  return 0;
}

/* skaluje zakres losowy  i zwraca liczbę losową*/
int nrand(int n) {
  if (n <= 0 || n > RAND_MAX) {
    std::cout << "Argument funkcji poza zakresem!" << std::endl;
    exit(EXIT_FAILURE);
  }

  const int bucket_size = RAND_MAX / n;
  int r;
  do {
    r = std::rand() / bucket_size;
  } while (r >= n);

  return r;
}

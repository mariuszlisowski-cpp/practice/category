// insertion sort
// ~ good for partially sorted data

#include <iostream>

using namespace std;

int * insertionSort(int arr[], int size) {
    // iterate all elements
    for (int i = 1; i < size; ++i) {
        // save current element
        int curr = arr[i];
        int j;
        // iterate from previous to beginning
        for (j = i - 1; j >= 0  ; --j) {
            // do nothing if current is bigger
            if (curr >= arr[j])
                break;
            // replace current with previous
            else
                arr[j + 1] = arr[j];
        }
        // insert replaced
        arr[j + 1] = curr;
    }
    return arr;		
}

int main() {
    int arr[] {4, 3, 2, 5, 7, 1, 9, -1, 4, 0};
    int size = sizeof(arr) / sizeof(*arr);

    insertionSort(arr, size);

    for (const auto& e : arr)
        cout << e << " ";

    return 0;
}
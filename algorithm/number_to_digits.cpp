/*
Number to digits
~ konwertuje liczbę całkowitą na poszczególne cyfry
*/

#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::string;

vector<int>* numberToDigits(int);
void displayVector(vector<int>);

int main() {
  int num;
  vector<int>* v;

  cout << "Input..." << endl << ": ";
  cin >> num;
  cout << "Output..." << endl;

  v = numberToDigits(num);
  displayVector(*v);
  delete v;

  return 0;
}

vector<int>* numberToDigits(int number) {
  vector<int>* digits;
  digits = new vector<int>;

  if (number == 0)
    digits->push_back(number);
  else {
    int digit;
    while(number) {
      digit = number % 10;
      digits->push_back(digit);
      number /= 10;
    }
  }
  return digits;
}

void displayVector(vector<int> vec) {
  vector<int>::const_iterator iter = vec.begin();
  while (iter != vec.end())
      cout << "digit: " << *iter++ << endl;
}

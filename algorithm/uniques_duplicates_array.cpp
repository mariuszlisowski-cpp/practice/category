#include <algorithm>
#include <iostream>
#include <iterator>
#include <unordered_set>
#include <vector>

int main() {
    int arr[] = {1, 5, 2, 1, 4, 3, 1, 7, 2, 8, 9, 5};

    std::unordered_set<int> uniques;
    std::unordered_set<int> duplicates;
    std::for_each(std::begin(arr), std::end(arr),
                  [&](auto el) {
                      if (uniques.find(el) != uniques.end()) {
                          duplicates.insert(el);
                      } else {
                          uniques.insert(el);
                      }
                  });
    for (auto el : duplicates) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
    for (auto el : uniques) {
        std::cout << el << ' ';
    }

    return 0;
}

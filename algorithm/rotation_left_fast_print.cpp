//

#include <iostream>

using namespace std;

void leftRotate(int [], int, int);
void rotateLeft(int [], int, int);
void showArray(int *, int );

int main() {
   // elements input
   int n;
   cout << "No of elements?" << endl << ": ";
   cin >> n;

   // array input
   int arr[n];
   for (int i =0; i < n; i++) {
      cout << "Element " << i+1 << ": ";
      cin >> arr[i];
   }

   // rotations input
   int d;
   cout << "How many left rotations? " << endl << ": ";
   cin >> d;

   // array display
   cout << "Before rotation..." << endl;
   showArray(arr, n);

   // rotation
   cout << "After rotation..." << endl;
   rotateLeft(arr, n, d);


  return 0;
}

void leftRotate(int arr[], int n, int d)
{
    /* To get the starting point of rotated array */
    int mod = d % n;

    // Prints the rotated array from start position
   for (int i = 0; i < n; i++)
      cout << (arr[(mod + i) % n]) << " ";
   cout << endl;
}

void rotateLeft(int arr[], int n, int d)
{
    /* To get the starting point of rotated array */
    int mod = d % n;

    // Prints the rotated array from start position
   for (int i = 0; i < n; i++)
      cout << (arr[(mod + i) % n]) << " ";
   cout << endl;
}

void showArray(int *arr, int n) {
   for (int i =0; i < n; i++)
      cout << arr[i] << " ";
   cout << endl;
}

#include <iostream>
#include <algorithm>

void print_array(int* arr, int size) {
    for (int i{}; i < size; ++i) {
        std::cout << arr[i] << ' ';
    }
    std::cout << std::endl;
}

void insertionSort(int *arr, int arrSize) {
    for (int i = 1; i < arrSize; ++i) {
        int picked = arr[i];
        int sorted;
        for (sorted = i - 1; sorted >=- 0; --sorted) {
            if (arr[sorted] > picked)
                arr[sorted + 1] = arr[sorted];                              // swap (part 1)
            else
                break;
        }
        arr[sorted + 1] = picked;                                           // swap (part 2)
    }
}

int main() {
    int arr[]{4, 3, 2, 0, 1, 5, 6};

    auto size = sizeof(arr) / sizeof(*arr);

    insertionSort(arr, size);
    print_array(arr, size);

    std::sort(arr, arr + size, std::greater{});                             // std::less{} by default
    print_array(arr, size);

    return 0;
}

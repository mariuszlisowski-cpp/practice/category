#include <iostream>

struct Node {
    Node(int data) : data{data}, next{nullptr} {}
    ~Node() {
        delete next;
    }
    Node* next;
    int data;
};
 
struct List {
    List() : head{nullptr} {};
    ~List() {
        delete head;
        // clear();
    }

    /* inserts to the head of the list for performance reasons
       this is an O(1) operation
          node          node          node                   temp
       |data|next|-> |date|next|-> |data|next|   <===push |data|next|
                                      head                         */
    void push(int data) {
        auto temp{ new Node(data)};
        if (!head) {
            head = temp;
        } else {
            temp->next = head;
            head = temp;
        }
    }

private:
    Node* head;
    void clear() {                                                          // not used
        while(head) {
            auto temp = head;
            head = head->next;
            delete temp;
        }
    }
};

int main() {
    List list;

    for (int i = 0; i < 200'000; ++i) {                                     // can cause stack overflow
        list.push(i);                                                       // for too many nodes
    }                                                                       // but should work for 100'000 nodes

    return 0;
}                                                                           // destroyed recursively

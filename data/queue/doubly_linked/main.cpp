// double queue
// by raisin

#include <iostream>
#include "dNode.hpp"
#include "dQueue.hpp"

using namespace std;

int main() {
    DQueue dqueue;
    dqueue.enqueueFront(-1);
    dqueue.enqueueFront(-2);
    dqueue.enqueueFront(-3);

    dqueue.enqueueBack(1);
    dqueue.enqueueBack(2);
    dqueue.enqueueBack(3);

    dqueue.showDQueue();

    dqueue.dequeueFront();
    dqueue.dequeueBack();

    dqueue.showDQueue();

    cout << "Front:" << dqueue.getFront() << endl;
    cout << "Back :" << dqueue.getBack() << endl;

    return 0;
}

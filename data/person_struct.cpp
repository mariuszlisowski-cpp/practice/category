// Person structure

#include <iostream>

using namespace std;

struct Person {
  string name;
  int age;
};

void showPerson(Person);
Person getOlder(Person&);

int main() {
  Person wife;
  wife.name = "Eve";
  wife.age = 39;
  showPerson(wife);

  Person son = { "Mat", 12 };
  showPerson(son);

  getOlder(wife);
  showPerson(wife);

  return 0;
}

void showPerson(Person p) {
  cout << "Name:\t" << p.name << endl;
  cout << "Age:\t" << p.age << endl;
  cout << "-----------" << endl;
}

Person getOlder(Person& p) {
  p.age++;
  return p;
}

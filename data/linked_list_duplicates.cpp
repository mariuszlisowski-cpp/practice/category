// linked list duplicates

#include <iostream>

using namespace std;

class Node {
public:
   int data;
   Node* next;
   Node(int d):data(d), next(NULL) {}
};

class LinkedList {
public:
   // inserts a new node at tail
   Node* insert(Node* head,int data) {
      Node* p = new Node(data);
      if(head == NULL)
         head=p;
      else if (head->next == NULL)
         head->next = p;
      else {
         Node* start = head;
         while (start->next != NULL)
            start = start->next;
         start->next = p;
      }
      return head;
   }

   // removes following duplicates
   Node* removeDuplicates(Node* head) {
      if (head == NULL)
         return head;
      Node* start = head;
      while (start) {
         if (start->next == NULL)
            return head; // end of list
         else if (start->data == start->next->data) {
            Node* temp = start->next;
            start->next = temp->next;
            delete temp;
            continue; // if more following duplicates
         }
         start = start->next; // no following duplicates
      }
      return head;
   }

   // traverse the list from head
   void display(Node* head) {
      if (head == NULL)
         return;
      Node* start = head;
      while (start) {
         cout << start->data << " ";
         start = start->next;
      }
      cout << endl;
   }
};

int main() {
   Node* head = NULL;
   LinkedList mylist;

   int arr[] = { 1, 1, 2, 2, 3, 3, 3, 4, 5, 6, 6 };
   size_t arrSize = sizeof(arr) / sizeof(*arr);

   for (int i = 0; i < arrSize; i++)
      head = mylist.insert(head,arr[i]);

   mylist.display(head);

   head = mylist.removeDuplicates(head);

   mylist.display(head);

   return 0;
}

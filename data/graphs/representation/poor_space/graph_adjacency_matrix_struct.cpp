/*  Representation: adjacency matrix
    O(V * V) space,  V - vertices, E - edges                    // WASTE (for zeroes)
    O(V) time (finding adjacent nodes)                          // OK
    O(1) time (finding if two nodes are connected)              // OK
    directed graph - vertex A is connected to B only
                     or B is connected to A only (as following)
    undirected graph - vertex A is connected to B 
                       and vice versa (as friendship)
 */
#include <forward_list>
#include <iostream>
#include <string>
#include <vector>

struct Vertex {
    Vertex(int index, const std::string& name)
        : index_(index), name_(name) {}
    std::string name_;
    int index_;
};

int main() {
    const int VERTICES_NO{ 8 };
    /* 8 vertexes, 10 edges, no weights
         A - B                                                  // indices 0 1
        / |  | \
        C D  E F                                                // indices 2 3 4 5
        |  \/ /
        G - H                                                   // indices 6 7
    */
    std::forward_list<Vertex> vertex_list{                      // 8 vertices (letters)
        {0, "A"},
        {1, "B"},
        {2, "C"},
        {3, "D"},
        {4, "E"},
        {5, "F"},
        {6, "G"},
        {7, "H"}
    };
    /* row: vortex index, col: indices of connected edges */
    int array[VERTICES_NO][VERTICES_NO]{
      // 0  1  2  3  4  5  6  7 
      // A  B  C  D  E  F  G  H
        {0, 1, 1, 1, 0, 0, 0, 0},                               // row 0 (A), connected to 1, 2, 3 (B, C, D)
        {1, 0, 0, 0, 1, 1, 0, 0},                               // row 1 (B), connected to 0, 4, 5 (A, D, F)
        {1, 0, 0, 0, 0, 0, 1, 0},                               // row 2 (C), connected to 0, 6 (A, G)
        {1, 0, 0, 0, 0, 0, 0, 1},                               // row 3 (D), connected to 0, 7 (A, H)
        {0, 1, 0, 0, 0, 0, 0, 1},                               // row 4 (E), connected to 1, 7 (B, H)
        {0, 1, 0, 0, 0, 0, 0, 1},                               // row 5 (F), connected to 1, 7 (B, H)
        {0, 0, 1, 0, 0, 0, 0, 1},                               // row 6 (G), connected to 2, 7 (C, H)
        {0, 0, 0, 1, 1, 1, 1, 0}                                // row 7 (H), connected to 3, 4, 5, 6 (D, E, F, G)
    };                                                          // diagonally symetric (search only in one half)
                                                                // array[i][j] == array[j][i] if undirected graph

    return 0;
}

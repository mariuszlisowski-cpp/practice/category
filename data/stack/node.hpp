#pragma once

class Node {
public:
    Node(int);

    int value;
    Node * Next;
};

Node::Node(int val):value(val), Next(nullptr) {}

// stack list class example

#include <iostream>

using namespace std;

class Element {
public:
   int data;
   Element* previous = NULL;

   Element(int d):data(d), previous(NULL) {}
};

class List {
public:
   Element* top = NULL;

   void addElement(int);
   void delElement();
   void showStack();
};

void List::addElement(int n) {
   Element* newEl;
   newEl = new Element(n); // new data added (via constructor)

   if (top == NULL)
      top = newEl; // adding first element
   else {
      newEl->previous = top; // current becomes previous
      top = newEl; // new top element
   }
}

void List::delElement() {
   if (top != NULL) {
      Element* temp = top; // current top becomes temp
      top = temp->previous; // top is now previous
      delete temp; // current top deleted
   }
   else
      cout << "Nothing to delete" << endl;
}

void List::showStack() {
   cout << "stack" << endl;
   Element* temp = top; // do not modify top pointer (use a copy)
   while (temp != NULL) {
      cout << " " << temp->data << endl << "---" << endl;
      temp = temp->previous;
   }
   cout << "NULL" << endl;
   delete temp;
}

void divider();

int main() {
   List* myList;
   myList = new List;
   divider();

   myList->addElement(1);
   myList->addElement(2);
   myList->showStack();
   divider();

   cout << "Deleting top..." << endl;
   myList->delElement();
   myList->showStack();
   divider();

   cout << "Deleting top..." << endl;
   myList->delElement();
   myList->showStack();
   divider();

   cout << "Deleting top..." << endl;
   myList->delElement();
   divider();

   return 0;
}

void divider() {
   cout << string(15, '-') << endl;
}

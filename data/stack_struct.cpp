// Stack
// ~ stucture stack implementation

#include <iostream>
#include <unistd.h> // system() method

using std::cin;
using std::cout;
using std::endl;

struct EStack {
  int data = 0;
  EStack* under = NULL;
};

void add2Stack(int d, EStack* &);
int removeFromStack(EStack* &);
void showStack(EStack*);

int main() {
  EStack* myStack = NULL;

  int l;
  char input;
  do {
    system("clear"); // clear screen
    cout << "Symulacja działania stosu:" << endl;
    cout << "1. Odkładanie na stos" << endl;
    cout << "2. Zdejmowanie ze stosu" << endl;
    cout << "Q - wyjście" << endl << endl;
    cout << "Aktualny stos:" << endl;;
    showStack(myStack);
    cout << endl << ": ";
    system("stty raw"); // entering character without hitting ENTER
    cin.get(input);
    system("stty cooked"); // restore terminal
    switch (input) {
      case '1': cout << endl << "Wartość odkładana?" << endl << ": ";
                cin >> l;
                add2Stack(l, myStack);
                cin.ignore(); // ignore 'enter' for the following 'cin'
                break;
      case '2': l = removeFromStack(myStack);
                if (l != 0) cout << endl << "Zdejmuję " << l << "..." << endl;
                else cout << endl << "Stos jest pusty!" << endl;
                sleep(1);
                break;
      case 'q': cout << endl << "Wychodzę..." << endl;
                 break;
      default:  cout << endl << "Błędny wybór!" << endl;
                sleep(1);
    }
  } while (input != 'q');

  return 0;
}

// append to stack
// passing by reference so changes top
void add2Stack(int d, EStack* &top) {
  EStack* newElement = new EStack;
  newElement->data = d;
  newElement->under = top; // current top becomes under
  top = newElement; // new top
}

// put away from stack
// passing by reference so changes top
int removeFromStack(EStack* &top) {
  if (top == NULL) return 0; // nothing to put away
  EStack* temp = top; // address copy
  int d = top->data;
  top = top->under;
  delete temp;
  return d;
}

// display stack
// passing by value so does not change top
void showStack(EStack* top) {
  while (top != NULL) {
    cout << top->data << endl << "|" << endl;
    top = top->under;
  }
  cout << "NULL" << endl;
}

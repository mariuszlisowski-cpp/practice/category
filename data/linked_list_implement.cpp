// linked list implementation
// (head)) a -> b -> c -> NULL (tail)
// #head_node #tail_node #next_node #insert_node #traverse
// #remove_head_node #remove_tail_node #remove_at_position

#include <iostream>

struct Node {
   char ch;
   Node* next = NULL;
};

using namespace std;

void insertNode(Node* &, char);
void removeHeadNode(Node* &);
void removeTailNode(Node* &);
void removeAtPos(Node* &, int);
void traverseList(Node* const &);

int main() {
   // creating an empty list
   Node* linkedList = NULL; // null head pointer

   // populating the list at tail
   for (char ch = 'A'; ch <= 'Z'; ch++)
      insertNode(linkedList, ch);

   // displaying the list
   traverseList(linkedList);

   // removing the nodes
   // ...anywhere
   removeAtPos(linkedList, 26); // delete the last
   removeAtPos(linkedList, 10);
   removeAtPos(linkedList, 1); // delete the first
   // displaying the list
   traverseList(linkedList);

   // ...at tail
   removeTailNode(linkedList); // delete the last
   // ...at head
   removeHeadNode(linkedList); // delete the first
   // displaying the list
   traverseList(linkedList);

  return 0;
}

// insert a new node at tail
void insertNode(Node* &head, char ch) {
   Node* newNode = new Node;
   newNode->ch = ch;
   // create the first node
   if (head == NULL)
      head = newNode;
   // create further nodes (at tail)
   else {
      // ...move from head
      Node* temp = head;
      // ...to tail
      while (temp->next != NULL)
         temp = temp->next; // the last node found
      // ...and insert at tail
      temp->next = newNode;
   }
}

// remove the node at head
void removeHeadNode(Node* &head) {
   if (head == NULL)
      return;
   Node* temp = head;
   head = head->next;
   delete temp;
}

// remove the node at tail
void removeTailNode(Node* &head) {
   // empty list
   if (head == NULL)
      return;
   // only one node
   Node* temp,* prev;
   if (temp->next == NULL) {
      temp = head;
      head = NULL;
      delete temp;
   }
   else {
      // finds the previous to the last node
      temp = head;
       while (temp->next != NULL) {
         prev = temp; // points to the previous to the last node
         temp = temp->next; // points to the last node
      }
      prev->next = NULL;
      delete temp;
   }
}

// remove the node at any position
void removeAtPos(Node* &head, int n) {
   Node* temp = head;
   // deletes head
   if (n == 1) {
      head = temp->next;
      delete temp;
   }
   else {
      // finds the pre-node of the node to be deleted
      for (int i = 0; i < n - 2; i++) {
         if (temp->next == NULL)
            return; // delete position out of range
         temp = temp->next; // points to the left neighbour
      }
      Node* curr = temp->next; // node to be deleted
      temp->next = curr->next; // linking the left & right
      delete curr;
   }
}

// traverse the list from the head
void traverseList(Node* const &head) {
   // empty list
   if (head == NULL)
      return;
   // non empty
   Node* temp = head;
   while (temp != NULL) {
      cout << temp->ch << " ";
      temp = temp->next; // points to NULL
   }
   cout << "NULL" << endl;
}

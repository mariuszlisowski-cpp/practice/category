#pragma once

class BSTNode {
public:
	int Key;
	BSTNode * Left;
	BSTNode * Right;
	BSTNode * Parent;
	BSTNode();
};

class BST : public BSTNode {
private:
	BSTNode * Root;
protected:	
	BSTNode * insert(BSTNode *, int);
public:
	BST();
	void insert(int);
};

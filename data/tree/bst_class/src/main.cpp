
// BST (binary search tree verbose)

#include <iostream>
#include "../inc/bst.hpp"

using namespace std;

void printSeparator(int);

int main() {
    int val;
    BST tree;

    ///////////////////////////////////////
    // add root
    val = 23;
    printSeparator(val);
    tree.insert(val); 	
    //   23
    ///////////////////////////////////////
    // add child (right)
    val = 31;
    printSeparator(val);
    tree.insert(val); 	
    //   23
    //     \
    //      31
    ///////////////////////////////////////
    // add child (right->left)
    val = 29;
    printSeparator(val);
    tree.insert(val); 	
    //   23
    //     \
    //      31
    //     /
    //   29
    ///////////////////////////////////////
    // add child (left)
    val = 12;
    printSeparator(val);
    tree.insert(val); 	
    //      23
    //     /  \
    //   12    31
    //        /
    //      29
    ///////////////////////////////////////
    // add child (left->left)
    val = 3;
    printSeparator(val);
    tree.insert(val); 	
    //      23
    //     /  \
    //   12    31
    //  /     /
    // 3    29
    ///////////////////////////////////////
    // add child (right->right)
    val = 88;
    printSeparator(val);
    tree.insert(val); 	
    //      23
    //     /  \
    //   12    31
    //  /     /  \
    // 3    29    88
    ///////////////////////////////////////

    return 0;
}

void printSeparator(int val) {
    cout << std::endl << "******** INSERT " << val << " *********" << endl;
}
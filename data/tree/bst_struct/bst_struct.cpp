// binary search tree
// yt/mycodeschool

#include <iostream>
#include <algorithm>
#include <queue>

using namespace std;
/////////////////////////////////////////////////////////////////
/// node
struct BstNode {
    int data;
    BstNode * left;
    BstNode * right;
};
/////////////////////////////////////////////////////////////////
/// insert
BstNode * createNewNode(int data) {
    BstNode * node = new BstNode;
    node->data = data;
    node->left = nullptr;
    node->right = nullptr;
    return node;
}
// pointer passed by a value
BstNode * insert(BstNode * node, int data) {
    if (!node)
        node = createNewNode(data);
    else if (data <= node->data)
        node->left = insert(node->left, data);
    else
        node->right = insert(node->right, data);
    return node;
}
/////////////////////////////////////////////////////////////////
/// find 
bool findValue(BstNode* node, int data) {
    if (!node)
        return false;
    else if (data == node->data)
        return true;
    else if (data < node->data)
        return findValue(node->left, data);
    else
        return findValue(node->right, data);
}
BstNode * findNode(BstNode* node, int data) {
    if (!node)
        return nullptr;
    else if (data == node->data)
        return node;
    else if (data < node->data)
        return findNode(node->left, data);
    else
        return findNode(node->right, data);
}
int findMinValue(BstNode * node) {
    if (!node)
        return -1;
    if (!node->left)
        return node->data;
    return findMinValue(node->left);
}
// returns a pointer to min value
BstNode * findMinNode(BstNode * node) {
    if (!node)
        return nullptr;
    if (!node->left)
        return node;
    return findMinNode(node->left);
}
int findMaxValue(BstNode * node) {
    if (!node)
        return -1;
    if (!node->right)
        return node->data;
    return findMaxValue(node->right);
}
// returns a pointer to max value
BstNode * findMaxNode(BstNode * node) {
    if (!node)
        return nullptr;
    if (!node->right)
        return node;
    return findMaxNode(node->right);
}
// height form the root
int findHeight(BstNode * node) {
    if (!node)
        return -1;
    int leftHight = findHeight(node->left);
    int rightHight = findHeight(node->right);
    return max(leftHight, rightHight) + 1;
}
/////////////////////////////////////////////////////////////////
/// traversal
void levelTraversal(BstNode * node) {
    if (!node)
        return;
    queue<BstNode *> q;
    q.push(node);
    while (!q.empty()) {
        BstNode * current = q.front();
        cout << current->data << " ";
        if (current->left)
            q.push(current->left);
        if (current->right)
            q.push(current->right);
        q.pop();
    }
    cout << endl;
}
void preorderTraversal(BstNode * node) {
    if (!node)
        return;
    cout << node->data << " ";
    preorderTraversal(node->left);
    preorderTraversal(node->right);
}
void postorderTraversal(BstNode * node) {
    if (!node)
        return;
    postorderTraversal(node->left);
    postorderTraversal(node->right);
    cout << node->data << " ";
}
// O(n)
void inorderTraversal(BstNode * node) {
    if (!node)
        return;
    inorderTraversal(node->left);
    cout << node->data << " ";
    inorderTraversal(node->right);
}
/////////////////////////////////////////////////////////////////
/// is tree a binary search tree
bool isSubtreeLesser(BstNode * node, int value) {
    if (!node)
        return true;
    if (node->data <= value &&
        isSubtreeLesser(node->left, value) &&
        isSubtreeLesser(node->right, value))
        return true;
    else
        return false;
}
bool isSubtreeGreater(BstNode * node, int value) {
    if (!node)
        return true;
    if (node->data > value &&
        isSubtreeGreater(node->left, value) &&
        isSubtreeGreater(node->right, value))
        return true;
    else
        return false;
}
// comparing all nodes [O(n^2)]
bool isBST(BstNode * node) {
    if (!node)
        return true;
    if (isSubtreeLesser(node->left, node->data) &&
        isSubtreeGreater(node->right, node->data) &&
        isBST(node->left) &&
        isBST(node->right))
        return true;
    else
        return false;
}
// comparing the range [O(n)]
bool isBinarySearchTree(BstNode * node, int minVal, int maxVal) {
    if (!node)
        return true;
    if (node->data > minVal &&
        node->data < maxVal &&
        isBinarySearchTree(node->left, minVal, node->data) &&
        isBinarySearchTree(node->right, node->data, maxVal))
        return true;
    else
        return false;
}
/////////////////////////////////////////////////////////////////
/// delete
BstNode * deleteNode(BstNode * node, int data) {
    if (!node)
        return nullptr;
    else if (data < node->data)
        node->left = deleteNode(node->left, data);
    else if (data > node->data)
        node->right = deleteNode(node->right, data);
    // found & ready to be deleted
    else {
        // no children
        if (!node->left && !node->right) {
            delete node;
            node = nullptr;
        }
        // one child
        else if (!node->left) {
            BstNode * temp = node;
            node = node->right;
            delete temp;
        }
        else if (!node->right) {
            BstNode * temp = node;
            node = node->left;
            delete temp;
        }
        // two children
        else {
            BstNode * temp = findMinNode(node->right);
            node->data = temp->data;
            node->right = deleteNode(node->right, temp->data);
        }
    }
    return node;
}
/////////////////////////////////////////////////////////////////
/// inorder successor [O(tree_height)]
BstNode * getSuccessor(BstNode * node, int data) {
    BstNode * current = findNode(node, data);
    if (!current)
        return nullptr;
    // node has right subtree
    if (current->right)
        return findMinNode(current->right);
    // no right subtree 
    else {
        BstNode * successor = nullptr;
        BstNode * ancestor = node;
        while (ancestor != current) {
            if (current->data < ancestor->data) {
                successor = ancestor;
                ancestor = ancestor->left;
            }
            else
                ancestor = ancestor->right;
        }
        return successor;
    }
}
/////////////////////////////////////////////////////////////////
/// main
int main() {
    BstNode * root = nullptr;

    // insert
    root = insert(root, 15);	
    root = insert(root, 10);	
    root = insert(root, 8);	
    root = insert(root, 12);	
    root = insert(root, 20);	
    root = insert(root, 17);	
    root = insert(root, 25);
    ////////////////////////
    //         15
    //       /   '\'
    //     10      20
    //    / '\'   / '\'
    //   8   12  17   25
    ////////////////////////	

    // search
    if (findValue(root, 20))
        cout << "Value found" << endl;
    else
        cout << "Value NOT found" << endl;

    // find
    cout << "Min: " <<  findMinValue(root) << endl;
    cout << "Min: " <<  findMinNode(root)->data << endl;
    cout << "Max: " <<  findMaxValue(root) << endl;
    cout << "Max: " <<  findMaxNode(root)->data << endl;
    cout << "Height: " << findHeight(root) << endl;

    // traversal
    cout << "Level:\t\t";
    levelTraversal(root);
    cout << endl;
    cout << "Preorder:\t";
    preorderTraversal(root);
    cout << endl;
    cout << "Postorder:\t";
    postorderTraversal(root);
    cout << endl;
    cout << "Inorder:\t";
    inorderTraversal(root);
    cout << endl;

    // is BST
    if (isBST(root))
        cout << "Is a BST" << endl;
    else
        cout << "Is NOT a BST" << endl;

    if (isBinarySearchTree(root, INT_MIN, INT_MAX))
        cout << "Is a BST" << endl;
    else
        cout << "Is NOT a BST" << endl;


    // cout << findMinNode(root->right)->data << endl;
    int val = 10;
    getSuccessor(root, val);
    cout << "Successor of " << val << ": " << 
             getSuccessor(root, val)->data << endl;

    root = deleteNode(root, 20);
    levelTraversal(root);

    return 0;
}
/*
          -1                level 0
        /     \
      -2       -3           level 1
     /   \    /  \
   -4     2  8    3         level 2
  /   \
-2     3                    level 3
*/
#include <iostream>

class BinaryTree {
public:
    int value;
    BinaryTree* left = nullptr;
    BinaryTree* right = nullptr;

    BinaryTree(int value) { this->value = value; }
};

int traverse_all_nodes(BinaryTree* node, int level) {
    if (!node) {
        return -1;
    }
    std::cout << "Level " << level << ": ";

    std::cout << "N:" << node->value << std::endl;                          // 'preorder traversal' if node listed here
    auto lhs = traverse_all_nodes(node->left, level + 1);
    auto rhs = traverse_all_nodes(node->right, level + 1);

    return std::max(lhs, rhs);
}

int main() {
    BinaryTree* tree = new BinaryTree(-1);

    tree->left = new BinaryTree(-2);
    tree->left->left = new BinaryTree(-4);
    tree->left->left->left = new BinaryTree(-2);
    tree->left->left->right = new BinaryTree(3);
    tree->left->right= new BinaryTree(2);

    tree->right = new BinaryTree(-3);
    tree->right->left = new BinaryTree(8);
    tree->right->right = new BinaryTree(3);

    int level_counter{0};
    auto output = traverse_all_nodes(tree, level_counter);
    // std::cout << output << std::endl;

    return 0;
}

// BST depth
// #binary_search_tree #height #detph #insert

#include <iostream>
#include <queue>

using namespace std;

class Node {
public:
   int data;
   Node *left;
   Node *right;
   Node(int d) {
      data = d;
      left = NULL;
      right = NULL;
   }
};

class BinarySearchTree {
public:
   Node* insert(Node* root, int data) {
      if (root == NULL) {
         return new Node(data);
      }
      else {
         Node* cur;
         if (data <= root->data) {
         cur = insert(root->left, data);
            root->left = cur;
         }
         else {
            cur = insert(root->right, data);
            root->right = cur;
         }
         return root;
      }
   }

   int getHeight(Node* root) {
      if (root == NULL)
         return -1;
      else {
         /* compute the depth of each subtree */
         int lDepth = getHeight(root->left);
         int rDepth = getHeight(root->right);
         /* return the larger one */
         if (lDepth > rDepth)
            return lDepth + 1;
         else
            return rDepth + 1;
      }
   }

   void levelOrder(Node* root) {
      if (root == NULL) return;
      queue<Node*> pQ; // queue of pointers
      pQ.push(root); // first pointer
      // traverse until the queue is empty
      while (!pQ.empty()) {
         Node* current = pQ.front();
         cout << current->data << " ";
         if (current->left != NULL)
            pQ.push(current->left); // left child to the queue
         if (current->right != NULL)
            pQ.push(current->right); // right child to the queue
         pQ.pop(); // removes fornt from the queue
      }
      cout << endl;
   }
};

int main() {
   BinarySearchTree myTree;
   Node* root = NULL;
   int t;
   int data;

   // tree entries input
   cout << "How many tree entries?" << endl << ": ";
   cin >> t;
   while (t--) {
      cout << "~: ";
      cin >> data;
      root = myTree.insert(root, data);
   }

   // calculating depth of the tree
   int height = myTree.getHeight(root);
   cout << "Max depht of the tree: " << height << endl;

   // printing level order of the tree
   cout << "Level order of the tree:" << endl;
   myTree.levelOrder(root);

   return 0;
}

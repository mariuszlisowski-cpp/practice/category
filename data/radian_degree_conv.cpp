// Radians degrees conversion

#include <iostream>
#include <cmath>

using namespace std;

struct Radian {
  double rad;
  int distance;
};

struct Degree {
  double deg;
  int distance;
};

void showRadian(Radian);
void showDegree(Degree);
Degree radian2Degree(Radian);
Radian degree2Radian(Degree);

const double rad2Deg = 180 / M_PI;
const double deg2Rad = M_PI/ 180;
int main() {
  Radian r;
  Degree d;
  double num;

  cout << "Conversion..." << endl;
  cout << "1. Radians to degrees" << endl;
  cout << "2. Degrees to radians" << endl;
  cout << ": ";

  int choice;
  cin >> choice;
  cout << "(enter): ";
  while (cin >> num) {
    switch (choice) {
      case 1: r.rad = num;
              d = radian2Degree(r);
              showDegree(d);
              break;
      case 2: d.deg = num;
              r = degree2Radian(d);
              showRadian(r);
              break;
    }
    cout << "(enter): ";
  }

  return 0;
}

void showRadian(Radian radians) {
  cout << radians.rad << " radians" << endl;
}

void showDegree(Degree degrees) {
  cout << degrees.deg << " degrees" << endl;
}

Degree radian2Degree(Radian radians) {
  Degree temp;
  temp.deg = radians.rad * rad2Deg;
  return temp;
}

Radian degree2Radian(Degree degrees) {
  Radian temp;
  temp.rad = degrees.deg * deg2Rad;
  return temp;
}

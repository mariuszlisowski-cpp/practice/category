// remove characters form string

#include <iostream>
#include <algorithm>

void removeChars(std::string &s, char p) {
    // shifting elements within range to the left leaving all
    // occurenes of 'value' at the end (returns past-the-end iterator)
    auto it = std::remove(s.begin(), s.end(), p);
    std::cout << "Shifted >" << s << "<" << std::endl;
    std::cout << "Last specified element: " << *(std::prev(it))
        << " (following unspecified values)" << std::endl;
    // erasing unwanted values left at the end
    s.erase(it, s.end());
}

int main() {
    std::string s {"9 1 9 2 9 3 9 4 9 5 9 6 9 7 9 8 9"};
    std::cout << s << std::endl;

    removeChars(s, ' ');
    std::cout << s << std::endl;

    std::cout << std::endl;

    removeChars(s, '9');
    std::cout << s << std::endl;

    return 0;
}

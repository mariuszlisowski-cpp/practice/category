// string split into words

#include <iostream>
#include <vector>
#include <boost/algorithm/string.hpp>

std::vector<std::string> splitIntoWords(std::string text) {
    std::vector<std::string> results;
    boost::split(results, text, [](char ch){return ch == ' ';}); // delimiter
    return results;
}

int main() {
    std::string sentence = "Let me split this into words !!! "; // ends with delimiter

    std::vector<std::string> words = splitIntoWords(sentence);

    for (auto e: words)
        std::cout << e << std::endl; // last element is an empty string

    std::cout << "last" << std::endl;

    return 0;
}

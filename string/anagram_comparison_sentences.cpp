// anagram comparison sentences
// ~ MUST have meaning

#include <iostream>
#include <algorithm>

using namespace std;

bool isAnagram(string s, string p) {
    // size does not matter
    // case insensitive
    transform(s.begin(), s.end(), s.begin(), ::tolower);
    transform(p.begin(), p.end(), p.begin(), ::tolower);

    // removing spaces
    s.erase(remove(s.begin(), s.end(), ' '), s.end());
    p.erase(remove(p.begin(), p.end(), ' '), p.end());

    // same sizes
    sort(s.begin(), s.end());
    sort(p.begin(), p.end());

    // verbose
    cout << s << endl;
    cout << p << endl;

    // same output?
    return (s == p);
}

int main() {
    string s1{ "Quid est veritas" };
    string s2{ "Vir est qui adest" };

    cout << (isAnagram(s1, s2) ? "Anagrams" : "NOT anagrams") << endl;

    return 0;
}

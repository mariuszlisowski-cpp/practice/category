#include <iostream>

using namespace std;

int main()
{
  /* zmienna napis ma znak '\0' na pozycji o indeksie 4
  (tekst dla 'napis' nie może być większy niż 9 znaków + '\0') */
  char napis[10] = "NAPIS"; // max 9 znaków (ostatni znak końca napisu to '\0')
  for (int i=0; i<10; i++)
  cout << napis[i] << " " << (int)(napis[i]) << endl;
  cout << endl;


  char txt1[] = "ONA"; // niejawna delaracja długości łańcucha
  cout << "Wyraz " << txt1 << " ma rozmiar " << sizeof(txt1) << ", a długość ciągu to " << strlen(txt1) << endl << endl; // rozmiar 4 (bo '\0' na końcu)

  /* STRING: deklaracja przez przypisanie znaków oraz znaku końca łańcucha */
  char txt2[] = {'A','B','C','\0'};
  cout << txt2 << endl << endl; // wyświetlany jako 'string'

  /* porównywanie napisów */
  char txt5[6] = "ONA";
  char txt6[6] = "ONA";
  if (!strcmp(txt5,txt6)) cout << "Łańcuchy takie same!" << endl << endl; // zwraca 0 dla równych

  /*  strcpy - kopiuje 'string' do tablicy 'char[]', ew. wskaźnika char* */
  strcpy(txt5,"MILO"); // txt5 ma rozmiar 6 elementów (z przykładu wcześniej)
  strcpy(txt6,"eMILO"); // txt6 ma rozmiar 6 elementów (tutaj wymagane min)
  cout << txt5 << " zasiąpione " << txt6 << ": " << strcpy(txt5,txt6) << endl << endl;

  /* kontaktenacja */
  strcat(txt5,txt6); // do txt5 doklejam txt6
  cout << txt6 << " doklejone: " << txt5 << endl;

  /* można tekst przypisać do wskaźnika na char, ale tylko stałą */
  const char* t = "NERO"; // readonly

  /* szukanie znaku w tekście */
  char* n = strchr(txt6,'L'); // zwraca wskaźnik do pierwszego 'L'
  cout << n << endl; // zatem zwróci resztę napisu
  if (!strchr(txt6,'y')) cout << "y nie występuje w " << txt6 << endl; // zwraca wskaźnik pusty (NULL), jak nie ma znaku w napisie

  /* odpowiednik 'find_first_of' */
  char txt7[] = "ROK_2019";
  char znaki[] = "1234567890";
  size_t z = strcspn(txt7,znaki); // size_t to alias do 'unsigned int'
  cout << txt7 << " " << z << " " << txt7[z] << endl; // zwraca pozycję  gdzie wystąpił pierwszy ze znaków

  /* szukanie podciągu */
  strcpy(txt5,"LATA 80-te");
  char* p = strstr(txt5,"80-te"); // zwraca wskaźnik do znalezionego miejsca
  cout << p << endl;
  if (!strstr(txt5,"50-te")) cout << "Nic nie znaleniono!" << endl; // NULL

  cout << endl;
  return 0;
}

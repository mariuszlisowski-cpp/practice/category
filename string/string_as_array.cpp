/*
String as array
#string to int conversion
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main()
{
  string s[2]= { "2|5", "3|6" };
  int tab[2][2];

  cout << (tab[0][0] = int(s[0][0] - '0')) << "|"
       << (tab[0][1] = int(s[0][2] - '0')) << endl;
  cout << (tab[1][0] = int(s[1][0] - '0')) << "|"
       << (tab[1][1] = int(s[1][2] - '0')) << endl;

  cout << tab[0][0] << " + " << tab[0][1] << " = "
       << int(s[0][0] - '0') + int(s[0][2] - '0') << endl;
  cout << tab[1][0] << " + " << tab[1][1] << " = "
       << int(s[1][0] - '0') + int(s[1][2] - '0') << endl;

  return 0;
}

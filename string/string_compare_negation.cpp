/* Return value of std::string::compare()
   - negative value if *this appears before the character sequence specified by the arguments, in lexicographical order
   - zero if both character sequences compare equivalent
   - positive value if *this appears after the character sequence specified by the arguments, in lexicographical order
*/
#include <iostream>
#include <string>

int main() {
    std::string strA{ "one" };
    std::string strB{ "one" };
    std::string strC{ "two" };

    std::cout << strA.compare(strB) << std::endl;               // zero (same)

    if (!strA.compare(strB)) {                                  // negation (!)
        std::cout << "same strings" << std::endl;               // same
    } else {
        std::cout << "different strings" << std::endl;
    }

    std::cout << strA.compare(strC) << std::endl;               // negative
    std::cout << strC.compare(strA) << std::endl;               // positive
    
    return 0;
}

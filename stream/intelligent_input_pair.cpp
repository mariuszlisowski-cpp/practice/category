/*
Intelligent input & search
*/

#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

int main() {
  vector<pair<int, int> > pairs;
  string s;
  int a, b;

  cout << "Enter a list of pairs of integers, one pair per line."
          "Press ENTER on a blank line to finish.\n> ";

  /* While there is input and the user has not simply pressed ENTER */
  while (getline(cin, s) && !s.empty()) {
    stringstream ss(s);
    ss >> a >> b;
    if (!ss)
      cout << "Invalid pair. Try again";
    else
      pairs.push_back(make_pair(a, b));
    cout << "> ";
  }

  /* Now get a single number which we'll use to search the list */
  cout << "Thank you. Now enter a number to find.\n> ";
  while (getline(cin, s)) {
    if (stringstream(s) >> a) break;
    cout << "Invalid integer. Try again.\n> ";
  }

  /* Count the number of times the user's number appears in the list */
  b = 0;
  typedef vector<pair<int, int> >::iterator iter;
  for (iter p = pairs.begin(); p != pairs.end(); p++) {
    if (p->first == a) b++;
    if (p->second == a) b++;
  }
  switch (b) {
    case 0:  cout << "That number does not appear in the list.\n"; break;
    case 1:  cout << "That number appears in the list 1 time.\n"; break;
    default: cout << "That number appears in the list " << b << " times.\n";
  }

  cout << "Thanks for playing. Bye!\n";
  return 0;
}

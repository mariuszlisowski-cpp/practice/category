/*
Sumy wielokrotne
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
  long int l, sum;
  long long int total = 0;

  while (!cin.eof()) {
    sum = 0;
    while (cin >> l) {
      if (l == 0)
        break;
      sum += l;
    }
    if (cin.good())
      cout << sum << endl;
    total += sum;
  }
  cout << total << endl;
  return 0;
}

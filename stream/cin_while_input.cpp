/*
cin while loop input
*/

#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main() {
  const int max = 3;
  int num[max];
  cout << "Enter " << max << " integer numbers..." << endl;
  for (int i = 0; i < max; i++) {
    cout << "Number " << i+1 << ": ";
    while (!(cin >> num[i])) { // if incorrect entry enter the loop
      cin.clear(); // clear cin error state
      while (cin.get() != '\n') // read and...
        continue; //  dismiss any remaining characters
      cout << "It's not a number! Try again..." << endl;
      cout << "Number " << i+1 << ": ";
    }
  }

  return 0;
}

// wide characters
// #wchar_t #wcout #wcin #wstream #wistream #wostream
// #wstringstream #wistringstream #wostringstream

#include <iostream>
#include <sstream>

using namespace std;

int main() {
  //
  wstringstream streamA;
  streamA << "Grace" << " " << "Sue" << " " << 39; // spaces between values

  wstring str = streamA.str();
  wcout << str << endl; // predefined instance of wostream class

  wstring name;
  streamA >> name; // ingoring the remaing content
  wcout << name << endl;

  return 0;
}

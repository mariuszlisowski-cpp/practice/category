// string split into words

#include <iostream>
#include <sstream>
#include <vector>
#include <iterator> // std::istream_iterator

std::vector<std::string> splitIntoWords(std::string text) {
    std::istringstream iss(text);
    std::vector<std::string> result(
            std::istream_iterator<std::string>{iss},
            std::istream_iterator<std::string>()
        );
    return result;
}

int main() {
    std::string sentence = "Let me split this into words !?! ?!?";

    std::vector<std::string> words = splitIntoWords(sentence);

    for (auto e: words)
        std::cout << e << std::endl;

    return 0;
}

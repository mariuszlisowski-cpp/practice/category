// read & write binary file

#include <iostream>
#include <fstream>

using namespace std;

class tempStat {
public:
  // data members
  double minimum, maximum;
  // constructor
  tempStat(double min = 0.0, double max = 0.0)
  : minimum(min), maximum(max) {}
  // helper method to write a tempStat object to a file in binary format
  void write(ostream & os) {
    os.write((char*)&minimum, sizeof(double));
    os.write((char*)&maximum, sizeof(double));
  }
  // helper method to read a tempStat object from a file in binary format
  void read(istream & is) {
    is.read((char*)&minimum, sizeof(double));
    is.read((char*)&maximum, sizeof(double));
  }
};

// operator >> overloading
istream& operator >> (istream& is, tempStat & ts) {
  is >> ts.minimum >> ts.maximum;
  return is;
}
// operator << overloading
ostream& operator << (ostream & os, const tempStat & ts) {
  os << ts.minimum << "," << ts.maximum << endl;
  return os;
}

void readTemperature(string);
void writeTemperature(string);

int main() {
  const string filename = "TempStat.bin";

  writeTemperature(filename);
  cout << endl;
  readTemperature(filename);

  return 0;
}

void writeTemperature(string filename) {
  ofstream ofile(filename, ios_base::binary);
  cout << "Writing text to file..." << endl;
  if (ofile.is_open()) {
    tempStat(-2.5, 7.5).write(ofile); // object instance & method call
    tempStat(0, 9.9).write(ofile);
    tempStat(2.5, 11.0).write(ofile);
    tempStat(5.5, 14.5).write(ofile);
    tempStat(7.0, 17.7).write(ofile);
    tempStat(10.5, 23.7).write(ofile);
    tempStat(11.7, 29.5).write(ofile);
    tempStat(7.6, 22.9).write(ofile);
    tempStat(7.2, 21.5).write(ofile);
    tempStat(2.0, 16.0).write(ofile);
    tempStat(-4.7, 12.5).write(ofile);
    tempStat(-1.9, 8.5).write(ofile);

    ofile.close();
    cout << "Finished writing binary data to " << filename << endl;
  }
  else
    cerr << "Couldn't open " << filename << " for writing" << endl;
}

void readTemperature(string filename) {
  ifstream ifile(filename, ios_base::binary);
  cout << "Reading text form file..." << endl;
  if (ifile.is_open()) {
    while (!ifile.eof()) {
      tempStat ts;
      ts.read(ifile);

      if (ifile.gcount() == 0) {
        break;  // If no bytes read, we're done.
      }
      cout << "Read temperature stats: " << ts;
    }
    ifile.close();
    cout << "Finished reading binary data from " << filename << endl;
  }
  else
    cerr << "Couldn't open " << filename << " for reading" << endl;
}

// stream double precision

#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

std::string convertDoubleToString(const long double valueToConvert, 
                                  const int precision)
{
    std::stringstream stream{};
    stream << std::fixed << std::setprecision(precision) << valueToConvert;

    return stream.str();
}

int main() {
    std::cout << 3.123456789 << '\n';
    std::cout << convertDoubleToString(3.123456789, 9) << '\n';

    return 0;
}

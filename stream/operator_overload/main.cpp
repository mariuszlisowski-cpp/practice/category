// operator overload example

#include "coordinates.hpp"
#include "position.hpp"

int main() {
    Coordinates coords;

    coords.writeToFile("coordinates");
    coords.readFromFile("coordinates");
    coords.writeToTerminal();

    return 0;
}

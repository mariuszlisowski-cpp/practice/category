// filestream name clearing

#include <fstream>
#include <iostream>
#include <string>

int main() {
    std::string filename;
    std::fstream file(filename, std::ios::in);
    bool success{ false };

    // filename reead
    while (!success) {
        std::cout << "\nEnter filename: ";
        std::cin >> filename;
        file.open(filename, std::ios::in);  // sets eofbit if fails
        
        if (!file) {
            std::cerr << "File open error!" << std::endl;
            // reset error before next try
            file.clear(file.rdstate() & ~std::ios::failbit);
            std::cout << "Try again..." << std::endl;
        } else {
            success = true;
            std::cout << "Success opening a file!" << std::endl;
        }
    }

    // byte search
    int number;
    std::ios::int_type character;
    do {
        std::cout << "\nEnter any byte number of the file: ";
        std::cin >> number;

        file.seekg(number);
        character = file.get(); // sets eofbit and failbit if fails

        if (file.eof()) {
            success = false;
            std::cerr << "File read error! Is byte number too high?" << std::endl;
            // reset errors before next try
            file.clear(file.rdstate() & ~(std::ios::eofbit | std::ios::failbit));
        } else {
            success = true;
            std::cout << "The byte no " << number << " is (hexadecimal): "
                      << std::showbase << std::hex
                      << static_cast<int>(character)
                      << std::endl;
        }
    } while (!success);


    return 0;
}
// number of lines in a file
// #fstream # ifstream

#include <iostream>
#include <fstream>

using namespace std;

int main() {
   string filename = "main.cpp";
   ifstream file;
   file.open(filename);

   if (!file.is_open()) {
      cout << "File open error! Exitting...";
      exit(EXIT_FAILURE);
   }

   int count = 0;
   string s;
   cout << "Opening file: " << filename << endl;
   cout << "*************************************" << endl;
   while (getline(file, s)) {
      cout << s << endl;
      count++;
   }
   cout << "*************************************" << endl;
   cout << "Number of lines: " << count << endl;

   return 0;
}

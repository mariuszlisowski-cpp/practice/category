#include "gtest/gtest.h"

#include "composite_integers.hpp"
#include "sum_up.hpp"

class Evaluate : public testing::Test {};

TEST_F(Evaluate, SimpleTest) {
    SingleValue single_value{11};
    ManyValues other_values;
    other_values.add(22);
    other_values.add(33);
    ASSERT_EQ(66, sum_up({&single_value, &other_values}));
}

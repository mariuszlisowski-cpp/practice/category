#pragma once

#include "composite_integers.hpp"
#include <vector>

int sum_up(const std::vector<ContainsIntegers*> items);

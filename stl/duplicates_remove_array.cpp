// duplicates removal from an array
// ~ remove all duplicates from an array

#include <iostream>
#include <unordered_set>

std::unordered_set<int> removeDuplicates(int arr[], int sz) {
    std::unordered_set<int> set(arr, arr + sz);
    return set;
}

void displaySet (std::unordered_set<int> set) {
    for (auto it : set)
        std::cout << it << "|";
}

int main() {
    int array[] {5, 5, 4, 4, 3, 3, 2, 2, 1};
    int size = sizeof(array) / sizeof(*array);

    std::unordered_set<int> singles;
    singles = removeDuplicates(array, size);

    displaySet(singles);

    return 0;
}

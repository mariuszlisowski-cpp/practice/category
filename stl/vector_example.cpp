// vector (Abstract Data Type list)
// #at #push_back #insert #erase #find #try #catch #distance

#include <iostream>
#include <vector>
#include <random>
#include <algorithm>

using namespace std;

template<class T>
void showArray(T array) {
    for (auto x : array)
        cout << x << endl;
}

int main() {
    vector<int> vec {10, 20, 30};
    vec.push_back(40);
    showArray(vec);

    // at() method throws exceptions
    try {
        cout << vec.at(3) << endl;
    }
    catch (out_of_range &range) {
        cerr << "Error: " << range.what();
    }

    // new array
    vector<float> list;
    for (auto i = 0; i < 5; i++)
        list.push_back(i + .25);

    vector<float>::iterator it;
    it = list.begin();
    list.insert(it, 3.14);
    list.insert(it + 1, 4.14);
    list.erase(it + 1);
    
    list.insert(list.end(), 9.99);
    showArray(list);

    // find a value
    float val = 3.14;
    it = find(list.begin(), list.end(), val);
    int index = distance(list.begin(), it);
    if (it != list.end()) {
         cout << *it << " found at position " << index;
    }
    else
        cout << val << " not found!";

    return 0;
}

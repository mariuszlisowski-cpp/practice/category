// transform ordinals
// #transform #toupper #back_inserter

#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

template<class T>
void display(const T &);

int main() {
    // transforming to uppercase
    string message("raisin");
    transform(message.begin(), message.end(), message.begin(),
        [](unsigned char c) -> unsigned char { return toupper(c); });

    // displaying
    display(message);

    // transforming to ASCII values
    vector<size_t> ordinals;
    transform(message.begin(), message.end(), back_inserter(ordinals),
        [](unsigned char c) -> size_t { return c; });

    // displaying
    display(ordinals);

    return 0;
}

template<class T>
void display(const T &el) {
    for (auto ch : el)
        cout << ch << '\t';
    cout << endl;
}

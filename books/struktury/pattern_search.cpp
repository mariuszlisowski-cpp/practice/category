// pattern search

#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::vector;
using std::cout;
using std::endl;

vector<int> searchPattern(string target, string pattern) {
    vector<int> vIndex;
    size_t targetLen = target.length();
    size_t patternLen = pattern.length();

    if (targetLen >= patternLen) {
        for (int i = 0; i <= targetLen - patternLen; ++i) {
            int j;
            for (j = 0; j < patternLen; ++j) {
                if (target[i + j] != pattern[j]) {
                    break;
                }
            }
            // above loop has not been broken
            if (j == patternLen) {
                vIndex.push_back(i);
                i += patternLen - 1; // jump after found pattern
            }
        }
    }

    return vIndex;
}

int main() {
    string target {"not is not and nothing else than not"};
    string pattern {"not"};
            
    vector<int> v = searchPattern(target, pattern);

    for (auto e : v) {
        cout << e << endl;
    }
    
    return 0;
}

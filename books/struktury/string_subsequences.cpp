// string subsequences 

#include <iostream>
#include <string>

bool isSubsequence(std::string str1, std::string str2, int x, int y) {
    if (x == 0) {
        return true;
    }
    if (y == 0) {
        return false;
    }
    if (str1[x-1] == str2[y-1]) {
        return isSubsequence(str1, str2, x - 1, y - 1);
    }
    else {
        return isSubsequence(str1, str2, x, y - 1);	
    }	
}

int main() {
    std::string str1 {"ab"};
    std::string str2 {"abcd"};
    
    if (isSubsequence(str1, str2, str1.size(), str2.size())) {
        std::cout << str1 << " jest podsekwencją ciągu " << str2 << std::endl;
    }
    else {
        std::cout << str1 << " NIE jest podsekwencją ciągu " << str2 << std::endl;
    }
        
     return 0;
}

// interpolation search
// for already sorted arrays
// two blocks (varied length)
// |....|..........| or
// |...........|...| or
// |......|........|
// worst O(log(log n))
// best O(1) (middle index)

#include <iostream>

using namespace std;

template<class T>
int interpolationSearch(T arr[], int lowIndex, int highIndex, T searchVal) {
    if (lowIndex <= highIndex) {
        int middleIndex = lowIndex + ((searchVal - arr[lowIndex]) * (highIndex - lowIndex) / (arr[highIndex] - arr[lowIndex]));
        if (searchVal == arr[middleIndex])
            return middleIndex;
        else if (searchVal < arr[middleIndex])
            return interpolationSearch(arr, lowIndex, middleIndex - 1, searchVal);
        else
            return interpolationSearch(arr, middleIndex + 1, highIndex, searchVal);
    }
    return -1;
}

int main() {
    int arr[] { 10, 20, 30, 40, 50, 60, 70, 80, 90 };
    int arrSize = sizeof(arr) / sizeof(*arr);

    int searched = 50;
    int foundIndex = interpolationSearch(arr, 0, arrSize - 1, searched);
    if (foundIndex != -1)
        cout << searched << " found at index " << foundIndex << endl;
    else
        cout << searched  << " not found!" << endl;

    return 0;
}

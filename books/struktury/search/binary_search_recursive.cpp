// binary search (recursion)
// for already sorted arrays
// two equal blocks
// |.......|.......|
// worst O(log n)
// best O(1) (middle index)

#include <iostream>

using namespace std;

template<class T>
int binarySearch(T arr[], int startIndex, int endIndex, T searchVal) {
    if (startIndex <= endIndex) {
        int middleIndex = startIndex + (endIndex - startIndex) / 2;
        if (searchVal == arr[middleIndex])
            return middleIndex;
        else if (searchVal < arr[middleIndex])
            return binarySearch(arr, startIndex, middleIndex - 1, searchVal);
        else
            return binarySearch(arr, middleIndex + 1, endIndex, searchVal);
    }
    return -1;
}

int main() {
    int arr[] { 10, 20, 30, 40, 50, 60, 70, 80, 90 };
    int arrSize = sizeof(arr) / sizeof(*arr);

    int searched = 50;
    int foundIndex = binarySearch(arr, 0, arrSize - 1, searched);
    if (foundIndex != -1)
        cout << searched << " found at index " << foundIndex << endl;
    else
        cout << searched  << " not found!" << endl;

    return 0;
}

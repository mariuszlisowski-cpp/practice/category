//  binary search

#include <iostream>

using namespace std;

bool binarySearch(int arr[], int arrSize, int num) {
    int indexMin {};
    int indexMax = arrSize - 1;

    while (indexMin <= indexMax) {
        int indexMid = indexMin + (indexMax - indexMin) / 2;
        // use left half of an array
        if (num < arr[indexMid])
            indexMax = indexMid - 1;
        // use right half of an array
        else if (num > arr[indexMid])
            indexMin = indexMid + 1;
        // found
        else 
            return true;
    }
    return false;
}

int main() {
    // array MUST BE sorted
    int numbers[] {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int size = sizeof(numbers) / sizeof(*numbers);

    if (binarySearch(numbers, size, 9))
        cout << "Found" << endl;
    else
        cout << "NOT found" << endl;	
    
    return 0;
}
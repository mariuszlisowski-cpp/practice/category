#include <iostream>

using namespace std;

class Animal
{
private:
    string m_name;
public:
    Animal(string name) : m_name(name) {}
    virtual string MakeSound() = 0;
    string GetName()
    {
        return m_name;
    }
};

class Dog : public Animal
{
public:
    Dog(string name) : Animal(name) {}
    string MakeSound() override
    {
        return "hau, hau!";
    }
};

int main()
{
    Dog dog = Dog("Nero");
    cout << dog.GetName() << " szczeka: ";
    cout << dog.MakeSound() << endl;
    return 0;
}

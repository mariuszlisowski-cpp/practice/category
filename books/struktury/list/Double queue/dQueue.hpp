// Front -> node -> Back (preferred)
// Head -> node -> Tail
// ADD Head: O(1)
// ADD Tail: O(1)
// REMOVE Head: O(1)
// REMOVE Tail: O(n)

#pragma once

class DQueue {
private:
    int count;
    DNode * Front; // Head
    DNode * Back; // Tail
public:
    DQueue();

    int getFront();
    int getBack();
    void enqueueFront(int); // add
    void enqueueBack(int); // add
    void dequeueFront(); // remove
    void dequeueBack(); // remove
    void showDQueue();
};

DQueue::DQueue():count(0), Front(nullptr), Back(nullptr) {}

int DQueue::getFront() {
    return Front->value;
}

int DQueue::getBack() {
    return Back->value;
}

void DQueue::enqueueFront(int val) {
    DNode * dNode = new DNode(val);
    if (count == 0) {
        Front = dNode;
        Back = dNode;
    }
    else {
        dNode->Prev = nullptr;
        dNode->Next = Front;
        Front->Prev = dNode;
        Front = dNode;
    }
    count++;
}

void DQueue::enqueueBack(int val) {
    DNode * dNode = new DNode(val);
    if (count == 0) {
        Front = dNode;
        Back = dNode;
    }
    else {
        dNode->Next = nullptr;
        dNode->Prev = Back;
        Back->Next = dNode;
        Back = dNode;
    }
    count++;
}

void DQueue::dequeueFront() {
    if (count == 0)
        return;
    DNode * dNode = Front;
    Front = dNode->Next;
    Front->Prev = nullptr;
    delete dNode;
    count--;
}

void DQueue::dequeueBack() {
    if (count == 0)
        return;
    DNode * dNode = Back;
    Back = dNode->Prev;
    Back->Next = nullptr;
    delete dNode;
    count--;
}

void DQueue::showDQueue() {
    DNode * dNode = Front;
    std::cout << "NULL <-> ";
    while (dNode) {
        std::cout << dNode->value << " <-> ";
        dNode = dNode->Next;
    }
    std::cout << "NULL" << std::endl;
}

#include "list.h"

List::List():m_count(0) {};

List::~List() {
    delete [] m_items;
    m_items = nullptr;
}

int List::get(int index) {
    if (index < 0 || index > m_count)
        return -1;
    return m_items[index];
}

void List::insert(int index, int value) {
    if (index < 0 || index > m_count)
        return;
    int * oldArray = m_items;
    m_count++;
    m_items = new int[m_count];
    for (int i = 0, j = 0; i < m_count; ++i) {
        if (index == i)
            m_items[i] = value;
        else {
            m_items[i] = oldArray[j];
            ++j;
        }
    }
    delete [] oldArray;
}

int List::search(int value) {
    for (int i = 0; i < m_count; ++i) {
        if (m_items[i] == value)
            return i;
    }
    return -1;
}

void List::remove(int index) {
    if (index < 0 || index > m_count)
        return;
    int * oldArray = m_items;
    m_count--;
    m_items = new int[m_count];
    for (int i = 0, j = 0; i < m_count; ++i, ++j) {
        if (index == j)
            ++j;
        m_items[i] = oldArray[j];
    }
    delete [] oldArray;
}

int List::count() {
    return m_count;
}

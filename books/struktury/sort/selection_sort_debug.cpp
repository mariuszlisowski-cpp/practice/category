// selection sort (debug)
// best & worst case O(n*n)

#include <iostream>

using namespace std;

void showArr (int arr[], int arrSize) {
    for (int i = 0; i < arrSize; ++i)
        cout << arr[i] << ' ';
    cout << endl;
}

// debug info can be omitted
void selectionSort(int *arr, int arrSize) {
    int minIndex, min {};
    do {
        minIndex = min;

        cout << "Start index: " << minIndex << endl; // debug
        showArr(arr, arrSize); // debug

        for (int i = min + 1; i < arrSize; ++i) {
            if (arr[minIndex] > arr[i]) {
                minIndex = i;
            }
        }
        cout << "Lowest: " << arr[minIndex] << endl; // debug
        cout << "Swapping : " << arr[min] << " <-> " << arr[minIndex] << endl; // debug
        swap(arr[min], arr[minIndex]);

        showArr(arr, arrSize); // debug
        cout << "Any key..." << endl; // debug
        cin.get(); // debug

        ++min;
    } while(min < arrSize - 1);
    cout << "No more swaps. Finished!" << endl; // debug
}

int main() {
    int arr[] { 50, 40, 30, 20, 10 };
    // int arr[] { 45, 25, 87, 36, 15, 33, 10 };
    int arrSize =  sizeof(arr) / sizeof(*arr);

    selectionSort(arr, arrSize);


    return 0;
}

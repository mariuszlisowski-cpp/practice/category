#include <iostream>
#include "../include/BST.hpp"
#include "../include/BSTNode.hpp"

BST::BST():root(nullptr) {}


// INSERT **************************************
void BST::Insert(int key) {
    root = Insert(root, key); // always root
}
BSTNode * BST::Insert(BSTNode * node, int key) {
    // new node
    if (!node) {
        node = new BSTNode;

        node->Key = key;
        node->Left = nullptr;
        node->Right = nullptr;
        node->Parent = nullptr;
    }
    // insert into right subtree
    else if (node->Key < key) {
        node->Right = Insert(node->Right, key);
        node->Right->Parent = node;
    }
    // insert into left subtree
    else {
        node->Left = Insert(node->Left, key);
        node->Left->Parent = node;
    }
    return node;
}
// REMOVE **************************************
void BST::Remove(int key) {
    root = Remove(root, key);
}
BSTNode * BST::Remove(BSTNode * node, int key) {
    // key not found
    if (!node)
        return nullptr;
    // key found
    if (node->Key == key) {
        // leaf (no children)
        if (!node->Left && !node->Right)
            node = nullptr;
        // one child at right
        else if (!node->Left &&  node->Right) {
            node->Right->Parent = node->Parent;
            node = node->Right;
        }
        // one child at left
        else if (node->Left && !node->Right) {
            node->Left->Parent = node->Parent;
            node = node->Left;
        }
        // two children
        else {
            int successorKey = Successor(key);
            node->Key = successorKey;
            node->Right = Remove(node->Right, successorKey);
        }
    }
    // search on right side
    else if (node->Key < key)
        node->Right = Remove(node->Right, key);
    // search on left side
    else
        node->Left = Remove(node->Left, key);
    
    return node;
}
// FIND ****************************************
int BST::FindMin() {
    return FindMin(root);
}
int BST::FindMin(BSTNode * node) {
    if (!node)
        return -1;
    else if (node->Left == nullptr)
        return node->Key;
    else
        return FindMin(node->Left);
}
int BST::FindMax() {
    return FindMax(root);
}
int BST::FindMax(BSTNode * node) {
    if (!node)
        return -1;
    else if (node->Right == nullptr)
        return node->Key;
    else
        return FindMax(node->Right);
}
// SUCC ****************************************
int BST::Successor(int key) {
    BSTNode * keyNode = Search(root, key);
    return keyNode ? Successor(keyNode) : -1;
}
int BST::Successor(BSTNode * node) {
    if (node->Right)
        return FindMin(node->Right);
    else {
        BSTNode * parentNode = node->Parent;
        BSTNode * currentNode = node;
        while (parentNode && currentNode == parentNode->Right) {
            currentNode = parentNode;
            parentNode = currentNode->Parent;
        }
        return parentNode ? parentNode->Key : -1;
    }
}
// PRED ****************************************
int BST::Predecessor(int key) {
    BSTNode * keyNode = Search(root, key);
    return keyNode ? Predecessor(keyNode) : -1;
}
int BST::Predecessor(BSTNode * node) {
    if (node->Left)
        return FindMax(node->Left);
    else {
        BSTNode * parentNode = node->Parent;
        BSTNode * currentNode = node;
        while (parentNode && currentNode == parentNode->Left) {
            currentNode = parentNode;
            parentNode = currentNode->Parent;
        }
        return parentNode ? parentNode->Key : -1;
    }
}
// SEARCH **************************************
bool BST::Search(int key) {
    BSTNode * result = Search(root, key);
    return result ? true : false;
}
BSTNode * BST::Search(BSTNode * node, int key) {
    if (!node)
        return nullptr;
    // key found
    else if (node->Key == key)
        return node;
    // search right branch
    else if (node->Key < key)
        return Search(node->Right, key);
    // search left branch
    else
        return Search(node->Left, key);
}
// PRINT ***************************************
void BST::PrintTreeInOrder() {
    PrintTreeInOrder(root); // always root
    std::cout << std::endl;
}
void BST::PrintTreeInOrder(BSTNode * node) {
    if (!node)
        return;
    PrintTreeInOrder(node->Left);
    std::cout << node->Key << " ";
    PrintTreeInOrder(node->Right);
}

void BST::PrintRoot() {
    std::cout << "root: " << root->Key << std::endl;
}
#pragma once

class BSTNode {
public:
	int Key;
	int Height;
	BSTNode * Left;
	BSTNode * Right;
	BSTNode * Parent;
};

#pragma once

#include "BSTNode.hpp"

class BST {
private:
	BSTNode * root;
protected:
	BSTNode * Insert(BSTNode *, int);
	BSTNode * Remove(BSTNode *, int);

	int FindMin(BSTNode *);
	int FindMax(BSTNode *);

	int Successor(BSTNode *);
	int Predecessor(BSTNode *);

	BSTNode * Search(BSTNode *, int);

	void PrintTreeInOrder(BSTNode *);
public:
	BST();

	void Insert(int);
	void Remove(int);
	
	int FindMin();
	int FindMax();

	int Successor(int);
	int Predecessor(int);

	bool Search(int);

	void PrintTreeInOrder();
	void PrintRoot();
};

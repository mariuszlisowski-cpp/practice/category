#include <iostream>

using namespace std;

class Animal
{
    private:
        string m_name;
    public:
        void GiveName(string name)
        {
            m_name = name;
        }
        string GetName()
        {
            return m_name;
        }
};

int main()
{
    Animal dog = Animal();
    dog.GiveName("Nero");
    cout << "Nazywam się " << dog.GetName() << endl;
    return 0;
}

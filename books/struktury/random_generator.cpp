// random numbers generator
// #for

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main() {
    srand(static_cast<unsigned int>(time(NULL)));
    for (int i=0; i<10; i++) {
        cout << (rand()  % 100 + 1) << " ";
    }
    cout << "\n";
    return 0;
}

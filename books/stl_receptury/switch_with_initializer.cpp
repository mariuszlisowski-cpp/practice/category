#include <cstdio>
#include <iostream>

// regular switch
int get_char_reg() {
    char c(static_cast<char>(std::getchar()));
    switch (c) {
    case 'a': return static_cast<int>(c);
    default:  return 0;
    }
}

// c++17
int get_char() {
    switch (char c(static_cast<char>(std::getchar())); c) {
    case 'a': return static_cast<int>(c);
    default:  return 0;
    }
}

int main() {
    std::cout << "> ascii: " << get_char() << std::endl;

    return 0;
}

#include <iostream>
#include <string>
#include <type_traits>
#include <vector>

template <typename T>
class addable {
    T val;
public:
    addable(T v) : val(v) {}

    template <typename U>
    T add(U x) const {
        if constexpr (std::is_same_v<T, std::vector<U>>) {      // if vector specialization
            auto copy{val};
            for (auto& el : copy) {
                el += x;
            }
            return copy;
        } else {                                                // other specialization
            return val + x;
        }
    }
};

int main() {
    std::vector<double> values{1.1, 1.2, 1.3};

    addable a{values};                                          // vector specialization
    auto added{a.add(1.0)};
    for (const auto& el : added) {
        std::cout << el << std::endl;
    }

    addable b{std::string("abc")};                              // string specialization
    std::cout << b.add(std::string("cde")) << std::endl;

    addable c{9};                                               // integer specialization
    std::cout << c.add(1) << std::endl;

    return 0;
}

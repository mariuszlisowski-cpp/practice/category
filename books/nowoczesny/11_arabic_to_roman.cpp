// konwersja liczb arabskich na rzymskie

#include <iostream>
#include <string>
#include <vector>

std::string toRoman(unsigned int value) {
    std::vector<std::pair<unsigned int, const char*>> roman {
        { 1000, "M" },
        {  900, "CM" },
        {  500, "D" },
        {  400, "CD" },
        {  100, "C" },
        {   90, "XC" },
        {   50, "L" },
        {   40, "XL" },
        {   10, "X" },
        {    9, "IX" },
        {    5, "V" },
        {    4, "IV" },
        {    1, "I" }
    };

    std::string result;
    for (const auto& pair : roman) {
        while (value >= pair.first) {
            result += pair.second;
            value -= pair.first;
        }
    }

    return result;
}

int main() {
    const unsigned int ROMAN_NUMBER = 1996;
    const unsigned int ROMAN_MAX_NUMBER = 3999; // MMMCMXCIX

    std::cout << toRoman(ROMAN_NUMBER) << std::endl;
    std::cout << toRoman(ROMAN_MAX_NUMBER) << std::endl;

    return 0;
}

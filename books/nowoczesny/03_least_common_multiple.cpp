// najmniejsza wspólna wielokrotność
// ~ najmniejsza dodatnia liczba całkowita, której dzielnikiem jest każda z nich

/* 
 *  lcm(a, b) = abs(a * b) / gcd(a, b)
 */

#include <iostream>
#include <numeric>
#include <vector>

unsigned int gcd(const unsigned int a, const unsigned int b) {
    return b == 0 ? a : gcd(b, a % b);
}

unsigned int lcm(const int a, const int b) {
    int h = gcd(a, b);
    return h ? a / h * b: 0; // more efficient to compute the lcm by dividing before multiplying
}

// numeric
template<typename InputIt>
unsigned int lcmr(InputIt first, InputIt last) {
    return std::accumulate(first, last, 1, lcm);
}

int main() {
    /* pair */
    int a = 6;
    int b = 21;

    std::cout << lcm(a, b) << std::endl;

    // numeric
    std::cout << std::lcm(a, b) << std::endl;

    /* multiple values */
    std::vector<int> vec {3, 6, 18, 32};
    
    std::cout <<  lcmr(vec.begin(), vec.end()) << std::endl;

    return 0;
}

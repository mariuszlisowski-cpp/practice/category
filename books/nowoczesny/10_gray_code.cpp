// kod Gray'a
// ~ dwie kolejne liczby różnią się od siebie tylko jednym bitem

/* 
 * decode:
 *  if b[i-1] == 1 then 
 *    g[i] = not b[i]
 *  else
 *    g[i] = b[i]
 * 
 * encode:
 *  b[0] = g[0]
 *  b[i] = g[i] xor b[i-1]
 */

#include <iostream>

const unsigned int BIT_SET = 32;

unsigned int gray_encode(const unsigned int num) {
    return num ^ (num >> 1); // xor
}

unsigned int gray_decode(unsigned int gray) {
    for (unsigned int bit = 1U << 31; bit > 1; bit >>= 1) {
        if (gray & bit) gray ^= bit >> 1;
    }

    return gray;
}

std::string to_binary(unsigned int value, int const digits) {
    return std::bitset<BIT_SET>(value).to_string().substr(BIT_SET - digits, digits);
}

int main() {
    std::cout << "Number\tBinary\tEncoded\t Decoded\n" << std::endl;

    for (unsigned int n = 0; n < BIT_SET; ++n) {
        auto encg = gray_encode(n);
        auto decg = gray_decode(encg);

        std::cout
            << n << "\t\t" << to_binary(n, 5) << "\t"
            << to_binary(encg, 5) << "\t\t" << decg << "\n";
    }

    return 0;
}

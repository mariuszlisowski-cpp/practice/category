#include <iostream>

int areaCube(int length, int width = 20, int height = 12); // parametry domyślne funkcji (parametr 'length' musi zostać przekazany do funkcji)

int main()
{
  int length = 100;
  int width = 50;
  int height = 2;
  int area;

  area = areaCube(length, width, height);
  std::cout << "1. Objętość wynosi: " << area << "\n";

  area = areaCube(length, width); // parametr 'height' ma domyślnie 12
  std::cout << "2. Objętość wynosi: " << area << "\n";

  area = areaCube(length); // parametr 'height' ma domyślnie 12 a 'width' 20
  std::cout << "3. Objętość wynosi: " << area << "\n";

  return 0;
}

int areaCube(int length, int width, int height)
{
  return (length * width * height);
}

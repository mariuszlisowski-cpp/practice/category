#include <iostream>

using namespace std;

class SimpleCat
{
public:
  SimpleCat();
  ~SimpleCat();
  int getAge() const { return *itsAge; }
  void setAge(int age) { *itsAge = age; }
  int getWeight() const { return *itsWeight; }
  void setWeight(int weight) { *itsWeight = weight; }
private:
  int *itsAge;
  int *itsWeight;
};

SimpleCat::SimpleCat()  // konstrukotr klasy
{
  itsAge = new int(2);
  itsWeight = new int(5);
}

SimpleCat::~SimpleCat() // destruktor klasy
{
  delete itsAge;
  delete itsWeight;
}

int main()
{
  SimpleCat *Frisky = new SimpleCat;
  cout << "\nFrisky ma lat " << Frisky->getAge() << " i waży " << (*Frisky).getWeight() << " kg";

  Frisky->setAge(4);      // wywołanie metody 'setAge' w obiekcie Frisky
  (*Frisky).setWeight(8); // wywołanie innej metody (odmienny zapis)

  cout << "\nFrisky ma lat " << Frisky->getAge() << " i waży " << (*Frisky).getWeight() << " kg";

  delete Frisky;
  cout << endl;
  return 0;
}

#include <iostream>

class Tricycle
{
public:
  Tricycle(int initialSpeed); // konstruktor obiektu
  ~Tricycle();  // destruktor obiektu
  int getSpeed() const // deklaracja metody jako 'inline'
  {
    return speed;
  }
  void setSpeed(int speed);
  void pedal() // deklaracja metody jako 'inline'
  {
    setSpeed(speed + 1);
    std::cout << "Jazda rowerem trójkołowym, szybkość wynosi " << getSpeed() << std::endl;
  }
  void brake() // deklaracja metody jako 'inline'
  {
    setSpeed(speed - 1);
    std::cout << "Jazda rowerem trójkołowym, szybkość wynosi " << getSpeed() << std::endl;
  }
private:
  int speed;
};

// Przekazywanie przez wartość albo wskaźnik
#include <iostream>
using namespace std;

class SimpleCat
{
public:
  SimpleCat();            // konstruktor
  SimpleCat(SimpleCat&);  // konstruktor kopiujący
  ~SimpleCat();           // destruktor
  int getAge() const { return itsAge; }
  void setAge(int age) { itsAge = age; }
private:
  int itsAge;
};

SimpleCat::SimpleCat()
{
  cout << "Konstruktor klasy SimpleCat..." << endl;
  itsAge = 10;
}

SimpleCat::SimpleCat(SimpleCat&) // klasa nie jest kopiowana i konstruktor ten nie jest wywoływany (wartość przekazana przez wskaźnik)
{
  cout << "Konstruktor kopiujący klasy SimpleCat..." << endl;
}

SimpleCat::~SimpleCat()
{
  cout << "Destruktor klasy SimpleCat..." << endl;
}

SimpleCat Function1(SimpleCat kot);
SimpleCat * Function2(SimpleCat * kot);

int main()
{
  cout << "\nUtworzenie obiektu..." << endl;
  SimpleCat Filemon;

  cout << "\nWywołanie funkcji 1..." << endl;
  Function1(Filemon);

  cout << "\nWywołanie funkcji 2..." << endl;
  Function2(&Filemon); // funkcja nie wywołuje konstruktorów i destruktora

  return 0;
}

SimpleCat Function1(SimpleCat kot) // przekazywanie przez wartość
{
  cout << "Zakończenie funkcji 1..." << endl;
  return kot;
}

SimpleCat * Function2(SimpleCat * kot) // przekazywanie przez wskaźnik
{
  cout << "Zakończenie funkcji 2..." << endl;
  return kot;
}

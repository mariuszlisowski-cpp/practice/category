#include <iostream>
#include <string> // biblioteka do pracy z ciągami tekstowymi

using namespace std;

int main()
{
  char string1[] = "Tutaj jest jakiś tekst.";
  char string2[80];
  char string3[10];

  strcpy(string2, string1); // kopiowanie tablicy 'string1' do bufora 'string2' (nie może być krótszy od tablicy 'string1')

  strncpy(string3, string1, 9); // kopiuje tylko 9 elementów tablicy 'string1'

  cout << "Ciąg 1: " << string1 << endl;
  cout << "Ciąg 2: " << string2 << endl;
  cout << "Ciąg 3: " << string3 << endl;
  return 0;
}

#include <iostream>

using namespace std;

class SimpleCat
{
public:
  SimpleCat() { itsAge = 2; } // implementacja 'inline' w kontstuktorze
  ~SimpleCat() {} // pusta implementacja w destruktorze
  int getAge() const { return itsAge; } // metoda klasy (nie może zmieniać)
  void setAge(int age) { itsAge = age; } // metoda klasy
private:
  int itsAge;
};

int main()
{
  SimpleCat *Frisky = new SimpleCat;
  cout << "\nFrisky ma lat " << Frisky->getAge(); // ekwiwalent (*Frisky).getAge() [wyłuskanie wartości]

  Frisky->setAge(4);
  cout << "\nFrisky ma lat " << Frisky->getAge(); // ekwiwalent (*Frisky).getAge()

  delete Frisky;
  cout << endl;
  return 0;
}

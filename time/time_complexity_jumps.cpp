// time complexity (jumps)

#include <cmath>
#include <iostream>

using namespace std;

int solution1(int X, int Y, int D);
int solution2(int X, int Y, int D);
int solution3(int X, int Y, int D);
int solution4(int X, int Y, int D);

int main() {
    int x = 1;
    int y = 1000000000;
    int d = 2;

    cout << "O(n) complexity:" << endl;
    cout << solution1(x, y, d) << endl;
    cout << "O(n) complexity:" << endl;
    cout << solution2(x, y, d) << endl;
    cout << "O(1) complexity:" << endl;
    cout << solution3(x, y, d) << endl;
    cout << "O(1) complexity:" << endl;
    cout << solution4(x, y, d) << endl;

    return 0;
}

// O(n) complexity [slow]
int solution1(int X, int Y, int D) {
    cout << "~ calculating..." << endl;
    int sum = X, count = 0;
    while (sum <= Y) {
        sum += D;
        ++count;
    }
    
    return count;
}

// O(n) complexity [slow]
int solution2(int X, int Y, int D) {
    cout << "~ calculating..." << endl;
    int i = 0;
    while (X + D * i <= Y) {
        i++;
    }
    return i;
}

// O(1) complexity [fast]
int solution3(int X, int Y, int D) {
    cout << "~ calculating..." << endl;
    
    return (int)ceil((Y - X) / (double)D);          // rounds to ceiling
}

// O(1) complexity [fast]
int solution4(int X, int Y, int D) {
    cout << "~ calculating..." << endl;
    int diff = Y - X;
    if (diff % D == 0) {
        return diff / D;                            // truncates decimals (returned ad int)
    } else {
        return diff / D + 1;                        // truncates decimals (returned ad int)
    }
}

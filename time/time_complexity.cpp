// time complexity
// #O(n*n) #O(n) #O(1)

#include <iostream>

using namespace std;

int main() {
    // 1 + 2 + 3 + 4 + 5 = 15
    int n = 5;
    int sum;

    // slowest [complexity O(n*n)]
    sum = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j < i + 1; j++) {
            sum += 1;
        }
    }
    cout << sum << endl;

    // faster [complexity O(n)]
    sum = 0;
    for (int i = 1; i <= n; i++) {
        sum += i;
    }
    cout << sum << endl;

    // fastest [complexity O(1)]
    cout << n * (n + 1) / 2 << endl;

    return 0;
}
